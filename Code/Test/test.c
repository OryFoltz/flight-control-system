#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <wiringPiSPI.h>
#include <signal.h>
#include <time.h>

void UARTSetup();
void BitBash();
void SendUART(char * data);
void ReceiveUART();
void SPISetup();
void WriteSPICHAR(char * data);
void WriteSPIINT(int data);
void MessageQueue(char * message);

void StartTimer(void);
void StopTimer(void);

void timer_callback (int sig)
{
	printf("Hey, signal went off! :D\n");
}

int uart_connection = -1;
int spi_connection = -1;

static const int SPI_CHANNEL = 1;

timer_t my_timer;

char * flight_status[10];
int new_message;

int main()
{
	printf("Entering test program...\n");

	int count = 0;
	int SPI_address = 1;
	char char_data;
	char * pchar_data = &char_data;
	int int_data = 0;
	new_message = 0;

	//wiringPiSetup();

	UARTSetup();
	//SPISetup();

	srand(time(NULL));

	//(void) signal(SIGALRM, timer_callback);
	//StartTimer();

	//char * test = malloc(strlen("Hi there"));
	//strcpy(test, "hi there");

	while (1)
	{
		int num = rand();

		while (num > 100)
			num = num - 100;

		char buffer[10];
		sprintf(buffer, "%d", num);
		SendUART(buffer);		
	}

	return 0;
}

void UARTSetup()
{
	printf("Initializing UART...\n");

	uart_connection = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY);
	
	if (uart_connection == -1)
		printf("Error - UNABLE TO OPEN UART\n");

	else
		printf("UART file descriptor result: %d\n", uart_connection);
	
	struct termios options;
	
	tcgetattr(uart_connection, &options);
	options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(uart_connection, TCIFLUSH);
	tcsetattr(uart_connection, TCSANOW, &options);
}

void BitBash()
{
	int count = 0;

	printf("Initializing bit bashing...\n");
	system("gpio -g mode 16 out");

	while (1)
	{
		system("gpio -g write 16 1");
		printf("Pin 16		HIGH\n");

		for (count  = 0; count < 1000; count++)
		{}

		system("gpio -g write 16 0");
		printf("Pin 16		LOW\n");

		for (count = 0; count < 1000; count++)
		{}
	}
}

void SendUART(char * data_str)
{
	printf("Sending: %s\n", data_str);

	if (uart_connection != - 1)
	{
		int size = write(uart_connection, data_str, strlen(data_str));
		
		if (size < 0)
			printf("ERROR - UART TX\n");
	}

	else
		printf("ERROR - FILESTREAM FAILED\n");
}

void ReceiveUART()
{
	unsigned char rx_buffer[166];

	if (uart_connection != -1)
	{
		int rx_length = read(uart_connection, (void*)rx_buffer, 165);		//Filestream, buffer to store in, number of bytes to read (max)
		if (rx_length < 0)
			printf("ERROR - RX ERROR OCCURED\n");

		else if (rx_length == 0)
			printf("ERROR - NO DATA WAITING\n");
		else
		{
			rx_buffer[rx_length] = '\0';
			printf("%i bytes read : %s\n", rx_length, rx_buffer);
		}
	}
}

void SPISetup()
{
	printf("Initializing SPI...\n");

	spi_connection = wiringPiSPISetup(SPI_CHANNEL, 500000);

	printf("SPI file descriptor result: %d\n", spi_connection);
}

void WriteSPICHAR(char * data)
{
	int result = 0;
	float rx_data = 0;
	int catch_data = 0;
	unsigned char rx_buffer[256];

	strcpy(rx_buffer, data);

	printf("Sending SPI data: %d\n", (int)rx_buffer[0]);

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection != -1)
		printf("Write successful!\n");

	else
		printf("Write failed! Error code: %s\n", result);

	catch_data = (int)rx_buffer[0];
	printf("Received SPI data: %d\n", catch_data);

	rx_data = catch_data * 0.019608;
	rx_data = (rx_data / 0.00957);
	
	printf("Calculated disance is: %f inches\n", rx_data);
}

void WriteSPIINT(int data)
{
	int result = 0;
	unsigned char rx_buffer[256];

	sprintf(rx_buffer, "%ld", data);

	printf("\n-------------------------------------------\n");
	printf("Sending SPI data: %s\n", rx_buffer);

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));


	if (spi_connection != -1)
		printf("Write successful!\n");

	else
		printf("Write failed! Error code: %s\n", result);

	printf("Received SPI data: %s\n", rx_buffer);
	printf("\n-------------------------------------------\n");
}

void StartTimer(void)
{
	struct itimerspec value;

	value.it_value.tv_sec = 1;
	value.it_value.tv_nsec = 0;

	value.it_interval.tv_sec = 1;
	value.it_interval.tv_nsec = 0;

	timer_create (CLOCK_REALTIME, NULL, &my_timer);
	timer_settime (my_timer, 0, &value, NULL);
}

void StopTimer(void)
{
	struct itimerspec value;

	value.it_value.tv_sec = 0;
	value.it_value.tv_nsec = 0;

	value.it_interval.tv_sec = 0;
	value.it_interval.tv_nsec = 0;

	timer_settime (my_timer, 0, &value, NULL);
}

void MessageQueue(char * message)
{
	new_message++;
	
	int count = 0;
	
	for (count = 9; count > 0; count--)
	{
		if (flight_status[count - 1]  != NULL)
		{
			free(flight_status[count]);
			flight_status[count] = (char *)malloc(strlen(flight_status[count - 1] ) );
			strcpy(flight_status[count], flight_status[count - 1]); 
		}
	}
	
	free(flight_status[0]);
	flight_status[0] = (char *)malloc(strlen(message));
	strcpy(flight_status[0], message);
}
