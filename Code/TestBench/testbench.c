/********************************************************************
*	File: 	testbench.c
*	Author:	Ory Foltz
*	Date:	3/17/17
*
*	Purpose: This file wil lhost the test bench program for the state 
*           statemachines of the FCS
*
*	Functions:  N/A
*
********************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <wiringPiSPI.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <softPwm.h>
#include <wiringPi.h>
#include <unistd.h>

void StandBy();
void TakeOff();
void Hover();
void Land();
void ELand();
void Critical();

void * PT_TestBench(void * arg);

//The flight "modes" of the FCS
enum flight_command 
{
	standby		= 0,
	takeoff 	= 1,
	hover 		= 2,
	flight 		= 3,
	land		= 4,
	e_land 		= 5,
	critical	= 6,
};

//The priority of sensors from objects
enum evasion_priority
{
	zero	= 0,	//>= 15 feet
	one		= 1,	//>= 10 feet
	two 	= 2,	//>= 8 feet
	three 	= 3, 	//>= 5 feet
	four	= 4,	//>= 2 feet
	five	= 5,	//>= 1.5 feet
	six		= 6, 	//>= 0 feet
};

//The value tied to a specific sensor
enum sensor_id
{
	front_l	= 0, //Front Left sensor
	left	= 1, //Left Sensor
	rear	= 2, //Rear Sensor
	posi	= 3, //Positive (+Z) sensor
	front_r	= 4, //Front Right sensor
	right	= 5, //Right sensor
	nega	= 6, //Negative (-Z) sensor
};

//A takeoff sequence the FCS executes
enum takeoff_sequence
{
	startup		= 0,
	check		= 1,
	initiate 	= 2,
	lift 		= 3,
	maintain	= 4,
	complete	= 5,
};

//A landing sequence the FCS executes
enum landing_sequence
{
	initializing 		= 0,
	confirm 		= 1,
	descent			= 2,
	berthing		= 3,
	berth			= 4,
};

/*************************************************
*	note: 
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*************************************************/
struct flight_control	
{
	enum flight_command 	command;
	enum evasion_priority	priority[7];
    
	int	ahead;
	int	astern;
	int	starboard;
	int	port;
	int	positive;
	int	negative;
	int 	stationary;
};

pthread_t test_bench;

struct flight_control FCS_data;
int system_up;
int stage_one;
int stage_two;
int stage_three;
int stage_four;
int stage_five;

int main ()
{
    system_up = 1;

    stage_one	    = 1; 
    stage_two	    = 1;
    stage_three	    = 1;
    stage_four	    = 1;
    stage_five	    = 1;
 
	//This is the thread will blocking wait for data to be sent from the user terminal
	int thread_error = pthread_create(&test_bench, NULL, &PT_TestBench, NULL); 
	
	if (thread_error != 0)
	{
	    printf("Thread creation failed...\n");
	    system_up = 0;
	}

	sleep(1);
 
    while (system_up)
    {
        //StandBy();
        //TakeOff();
        //Hover();
        Land();
        //ELand();
        //Critical();
        sleep(1);
    }
    
    return 0;
}

/*********************************************************************
*	Function: StandBy
*
*	Purpose:  This function will execute the standby mode for the FCS
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void StandBy()
{
	printf("SYSTEM: In STANDBY\n");

	FCS_data.command 	= standby;
	FCS_data.ahead 	= 0;
	FCS_data.astern 	= 0;
	FCS_data.starboard 	= 0;
	FCS_data.port 		= 0;
	FCS_data.positive 	= 0;
	FCS_data.negative 	= 0;
	FCS_data.stationary	= 1;
}

/*********************************************************************
*	Function: TakeOff
*
*	Purpose:  This function will execute the takeoff command if viable
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void TakeOff()
{
	int executing = 0;
	int count = 0;
	int flag = 0;
	enum takeoff_sequence takeoff_commands = startup;

	executing = 1;

	while(executing)
	{
		//Switch based on the current takeoff command attempting to be executed
		switch (takeoff_commands)
		{
			//This case statement will let the systems know a takeoff is initiating
			case startup:
			
				printf("TAKEOFF: Initializing...\n");
				takeoff_commands = check; 
			break;

			//This statement will check the value of the surrounding area to make sure a takoff is 
			//within the tolerances
			case check:	
				
				printf("TAKEOFF: Checking distances...\n");
				//Iterate through the sensors and make sure they meet the threshold 
				//for takeoff: Do not iterate through the negative sensor [6]
				for (count = 0, flag = 0; count < 6 && !flag; count++)
				{
					//If not the vertical sensor
					if (count != 3)
					{	
						//If item is closer then 2 feet (priority 5)
						if(FCS_data.priority[count] > four)
							flag = 1;
					}

					//else vertical sensor
					else
					{
						//If item is closer then 10 feet
						if (FCS_data.priority[count] > one)
							flag = 1;
					}
				}

				//If clear
				if (!flag)
				{
					printf("TAKEOFF: Distance checking successful...\n");
					takeoff_commands = initiate;
					stage_one = 0;
					sleep(1);
				}

				//If not clear
				else
				{
					printf("TAKEOFF: Distance checking failed... Returning to STANDBY\n");
					FCS_data.command = standby;
					executing = 0;
					stage_one = 0;
					sleep(1);
				}
				
				flag = 0;
			break;

			//Begin takeoff
			case initiate:
				
				//Turn on motors
				printf("TAKEOFF: Activating motors...\n");
				
				FCS_data.ahead 				= 0;
				FCS_data.astern 			= 0;
				FCS_data.starboard 			= 0;
				FCS_data.port 				= 0;
				FCS_data.positive 			= 1;
				FCS_data.negative 			= 0;
				FCS_data.stationary 		= 0;
				
				takeoff_commands = lift;
			break;

			//Monitor the first 2 feet of takeoff
			case lift:
			
				//Iterate through all of the sensors
				for (count = 0; count < 7; count++)
				{
					//If positive z axis
					if (count == 3)
					{
						//If closer then 2 feet
						if (FCS_data.priority[count] > four)
						{
							printf("TAKEOFF: Phase 1 of takeoff -positive- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
							stage_two = 0;
							sleep(1);
						}
					}
					
					//If negative z axis
					else if (count == 6)
					{
						//If two feet off of the ground, complete stage, move to maintain
						if (FCS_data.priority[count] == four)
						{
							printf("TAKEOFF: Phase 1 of takeoff complete...\n");
							takeoff_commands = maintain;
							stage_two = 0;
							sleep(1);
						}
					}
					
					//If any x/y axis
					else
					{
						//If object ever gets closer then 2 feet, go to emergency landing mode
						if (FCS_data.priority[count] > four)
						{
							printf("TAKEOFF: Phase 1 of takeoff -horizontal- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
							stage_two = 0;	
							sleep(1);
						}
					}
				}
	
			break;

			//Monitor takeoff from 2 -> 5 feet
			case maintain:
			
				//Iterate through all of the sensors
				for (count = 0; count < 7; count++)
				{
					//If positive z axis
					if (count == 3)
					{
						//If closer then 2 feet
						if (FCS_data.priority[count] > four)
						{
							printf("TAKEOFF: Phase 2 of takeoff -positive- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
							stage_three = 0;
							sleep(1);
						}
					}
					
					//If negative z axis
					else if (count == 6)
					{
						//If five feet off of the ground, complete stage, move to complete
						if (FCS_data.priority[count] == three)
						{
							printf("TAKEOFF: Phase 2 of takeoff complete...\n");
							takeoff_commands = complete;
							stage_three = 0;
							sleep(1);
						}
					}
					
					//If any x/y axis
					else
					{
						//If object ever gets closer then 5 feet, go to emergency landing mode
						if (FCS_data.priority[count] > three)
						{
							printf("TAKEOFF: Phase 2 of takeoff -horizontal- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
							stage_three = 0;
							sleep(1);
						}
					}
				}
			break;

			case complete:
			
				//Bring motors to hover
				printf("TAKEOFF: Takeoff successful... Initiating HOVER\n");
				FCS_data.command = hover;
				FCS_data.ahead 		= 0;
				FCS_data.astern 	= 0;
				FCS_data.starboard 	= 0;
				FCS_data.port 		= 0;
				FCS_data.positive 	= 0;
				FCS_data.negative 	= 0;
				FCS_data.stationary	= 1;
				executing 		= 0;
			break;

			default:
			
				//Critical error
				printf("TAKEOFF: Critical error... Initiating E_LAND\n");
				FCS_data.command = e_land;
				executing = 0;
			break;
		}		
	}

	while(1);
}

/*********************************************************************
*	Function: Hover
*
*	Purpose:  This function will execute the hover command
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void Hover()
{
	int count = 0;
	int l_lock = 0;
	int r_lock = 0;
	
	//This will iterate through all of sensors and check their
	//priorities and act accordingly
	for (count = 0; count < 7; count++)
	{
		
		//Switch based off of what sensor we are looking atan
		switch (count)
		{
			//Front Left Sensor
			case 0:
				
				//If sensor detects something closer then 8 feet and astern hasn't been set already
				if (FCS_data.priority[count] > two && FCS_data.astern == 0)
				{
					FCS_data.astern = 1;	
					l_lock = 1;
				}					
				
				//If not set by the othher forward sensor
				else if (!r_lock)
				{
					FCS_data.astern = 0;	
					l_lock = 0;
				}					
			break;
			
			//Left Sensor
			case 1:
			
				//If sensor detects something closer then 8 feet
				if (FCS_data.priority[count] > two)
					FCS_data.starboard = 1;
				
				else
					FCS_data.starboard = 0;
			break;
			
			//Rear Sensor
			case 2:
			
				//If sensor detects something closer then 8 feet
				if(FCS_data.priority[count] > two)
					FCS_data.ahead = 1;
				
				else
					FCS_data.ahead = 0;
			break;
			
			//Positive Sensor
			case 3: 
			
				//If the positive sensor detects something closer then 8 feet 
				if (FCS_data.priority[count] > two)
					FCS_data.negative = 1;
				
				else	
					FCS_data.negative = 0;
			break;
			
			//Front Right Sensor
			case 4:
			
				//If sensor detects object closer then 8 feet and stern hasnt been set
				if (FCS_data.priority[count] > two && FCS_data.astern == 0)
				{
					FCS_data.astern = 1;
					r_lock = 1;
				}
				
				//If the other forward sensor hasnt set it
				else if (!l_lock)
				{
					FCS_data.astern = 0;
					r_lock = 0;
				}
			break;
			
			//Right Sensor
			case 5:
			
				//If the right sensor detects something closer then 8 feet
				if (FCS_data.priority[count] > two)
					FCS_data.port = 1;
				
				else
					FCS_data.port = 0;
			break;
			
			//Negative Sensor
			case 6:
			
				//If the negative sensor detects something closer then 8 feet
				if (FCS_data.priority[count] > two)
					FCS_data.positive = 1;
				
				else
					FCS_data.positive = 0;
			break;

			default:
				//Critical error
				printf("HOVER: Critical error... Initiating E_LAND\n");
				FCS_data.command = e_land;
				count = 7;
			break;				
		}
	}
}

/*********************************************************************
*	Function: Land
*
*	Purpose:  This function will execute the Land command
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
**********************************************************************/
void Land()
{
	enum landing_sequence landing = initializing;
	int executing = 1;
	int count = 0;
	
	while (executing)
	{
		switch (landing)
		{
			case initializing:
				
				//Signal start of landing protocols
				printf("LAND: Initializing...\n");
				landing = confirm;
			break;
			
			case confirm:
			
				//Checking if ground is farther then 5
				if (FCS_data.priority[nega] < three)
				{
					landing = descent;
					printf("LAND: Beginning descent...\n");
				}
				
				//If ground is closer then 5 feet
				else
				{
					landing = berthing;
					printf("LAND: Berthing...\n");
				}

				stage_one = 0;
				sleep(1);
				
			break;
			
			case descent:
				
				printf("DESCENT\n");

				FCS_data.negative = 1;
				
				while ((landing == descent) && executing)
				{

				    //If ground is closer then 5 feet
				    if (FCS_data.priority[nega] > 3)
						landing = berthing;
				
				//Check priorities of other sensors while landing
				    for ( count = 0; count < 6; count++)
				    {
					//If anything is closer then 2 feet
					    if (FCS_data.priority[count] > four)
					    {
						    printf("LAND: Descent interference... Initiating E_LAND\n");
						    executing = 0;
						    FCS_data.command = e_land;
					    }
				    }
				}

				stage_two = 0;
				sleep(1);
					
			break;
			
			case berthing:
			
				printf("BERTHING\n");

				FCS_data.negative = 1;
				
				while((landing == berthing) && executing)
				{

				    //If ground is closer then two feet, narrow peripheral checks
				    if (FCS_data.priority[nega] > four)
				    {
					//If anything is closer then 2 feet above it, initiate emergency landing
					if (FCS_data.priority[posi] > four)
					{
					    printf("LAND: Berthing interference... Initiating E_land\n");
					    executing = 0;
					    FCS_data.command = e_land;
					}
					
					//If the ground is close enough to complete the landing
					if (FCS_data.priority[nega] >= six)
						landing = berth;
				    }
				
				    //Else maintain same checks as in descent
				    else
				    {
					for ( count = 0; count < 6; count++)
					{
					    //If anything is closer then 2 feet
					    if (FCS_data.priority[count] > four)
					    {
					    	printf("LAND: Berthing interference... Initiating E_LAND\n");
						executing = 0;
						FCS_data.command = e_land;
					    }
					}	
				    }
				}

				stage_three = 0;
				sleep(1);
			
			break;
			
			case berth:
			
				printf("LAND: Landing complete... Initializing STANDBY\n");
				
				FCS_data.negative = 0;
				FCS_data.command = standby;
				executing = 0;
			break;
			
			default:
            
				//Critical error
				printf("LAND: Critical error... Initiating E_LAND\n");
				FCS_data.command = e_land;
				executing = 0;
			break;
		}
	}

	printf("LAND complete\n");
	while (1);
}

/*********************************************************************
*	Function: ELand
*
*	Purpose:  This function will execute the ELand command. In this
*				lockout state the FCS is only concerned with landing
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
**********************************************************************/
void ELand()
{
	
	printf("E_LAND: Emergency Land intiated...\n");
	//While the ground is 1.5 feet away
	while (FCS_data.priority[6] < six)
	{
		//Iniate a controlled fall to the ground
		FCS_data.negative 	= 1;
		FCS_data.ahead 		= 0;
		FCS_data.astern 	= 0;
		FCS_data.starboard 	= 0;
		FCS_data.port 		= 0;
		FCS_data.positive 	= 0;
		FCS_data.stationary = 0;
	}
	
	FCS_data.negative 	= 0;
	FCS_data.stationary	= 1;
	
	printf("E_LAND: Emergency Land completed... Initiating STANDBY\r");
	FCS_data.command = standby;
}

/*********************************************************************
*	Function: Critical
*
*	Purpose:  This function will only be entered if a critical system 
*				error has occured and the FCS is no longer functional.
*				The Critical mode will hold the program, ending threads 
*				and tasks that wre not needed. The FCS must be hard reset
*				after entering the Critical state. 
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
**********************************************************************/
void Critical()
{
	int count = 0;
	
	//Message the system a critical error has occured
	printf("CRITICAL: A critical system error has occured... Holding in CRITICAL\n");	
	
	//Indefinetely clear values
	while (1)
	{
		FCS_data.negative 	= 0;
		FCS_data.ahead 		= 0;
		FCS_data.astern 	= 0;
		FCS_data.starboard 	= 0;
		FCS_data.port 		= 0;
		FCS_data.positive 	= 0;
		FCS_data.stationary	= 0;
		
		for (count = 0; count < 7; count++)
			FCS_data.priority[count] = zero;
		
		FCS_data.command = critical;
	}
}

void * PT_TestBench(void * arg)
{

    //This is used for lockout states such as takeoff and land

    //Stage one
    printf("STAGE ONE\n");
    while (stage_one)
    {
	FCS_data.priority[0] = 3;
	FCS_data.priority[1] = 2;
	FCS_data.priority[2] = 1;
	FCS_data.priority[3] = 1;
	FCS_data.priority[4] = 0;
	FCS_data.priority[5] = 5;
	FCS_data.priority[6] = 2;
    }

    printf("STAGE TWO\n");
    while (stage_two)
    {
	FCS_data.priority[0] = 0;
	FCS_data.priority[1] = 0;
	FCS_data.priority[2] = 0;
	FCS_data.priority[3] = 0;
	FCS_data.priority[4] = 0;
	FCS_data.priority[5] = 0;
	FCS_data.priority[6] = 3;

	sleep(3);

	FCS_data.priority[6] = 4;

	while(stage_two);
    }

    printf("STAGE THREE\n");
    while (stage_three)
    {
	FCS_data.priority[0] = 4;
	FCS_data.priority[1] = 0;
	FCS_data.priority[2] = 0;
	FCS_data.priority[3] = 4;
	FCS_data.priority[4] = 4;
	FCS_data.priority[5] = 0;
	FCS_data.priority[6] = 0;

	sleep(3);

	FCS_data.priority[6] = 5;

	sleep(3);

	FCS_data.priority[0] = 4;
	FCS_data.priority[1] = 0;
	FCS_data.priority[2] = 0;
	FCS_data.priority[3] = 4;
	FCS_data.priority[4] = 4;
	FCS_data.priority[5] = 0;
	FCS_data.priority[6] = 6;


	while(stage_three);
    }

    

    while (1);
}
