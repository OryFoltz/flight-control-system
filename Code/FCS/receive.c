#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>

void SignalSetup();
void ExitHandler(int signal);
void SerialSetup();
void FileSetup();
void PushMessage(char * message);
void PopMessage();
void CheckMessage(char * message);

void * PT_DisplayData(void * arg);
void * PT_GetInput(void * arg);

pthread_t display_data;
pthread_t user_input;

time_t raw_time;
struct tm * time_info;

struct FCS_message
{
	char * message;
	struct FCS_message * next;
};
struct FCS_message * m_head = NULL;
struct FCS_message * m_tail = NULL;
struct FCS_message * travel = NULL;

int uart_connection 	= -1;
int system_up 		= 1;
int read_status 	= 0;
int nodes_in_list 	= 0;
int PT_display_up	= 1;
int new_message		= 0;
float sensor_data[7];
FILE * file_descriptor = NULL;

int main()
{
	char buffer[1024];
	memset(buffer, 0, sizeof(buffer));
	int count 		= 0;	
	int thread_error 	= 0;
	char * token = NULL;

	if (system_up)
		SignalSetup();

	if (system_up)
		SerialSetup();

	if (system_up)
		FileSetup();

	thread_error = pthread_create(&display_data, NULL, &PT_DisplayData);

	if (thread_error != 0)
	{
		fputs("ERROR - Send Thread not created... Shutting down\n", file_descriptor);
		printf("ERROR - Send Thread not created... Shutting down\n");
		system_up = 0;
	}

	if (system_up)
		printf("Recieving FCS transmissions:\n--------------------------------------\n");

	while(system_up)
	{
		read_status = read(uart_connection, buffer, sizeof(buffer));

		if (read_status < 0)
		{
			printf("FAILURE - reading off of port ttyUSB0 failed... Shutting down\n");
			system_up = 0;
		}

		else
		{
			token = strtok(buffer, "\n");
			while ( token != NULL)
			{
				time (&raw_time);
				time_info = localtime(&raw_time);;

				//If not sensor data
				if (token[0] != '*')
				{
					fputs(asctime(time_info), file_descriptor);
					fputs(token, file_descriptor);
					fputs("\n---------------------------------------------------------------------\n", file_descriptor);
					PushMessage(token);
					new_message++;
				}

				CheckMessage(token);

				token = strtok(NULL, "\n");
			}

			memset(buffer, 0, sizeof(buffer));
		}
	}

	if (file_descriptor != NULL)
		fclose(file_descriptor);

	return 0;
}

/*******************************************************************
*	Function: SignalSetup
*
*	Purpose: This function sets up the forced exit signal
*
*	Inputs:		N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		Setup Provided by:
* http://stackoverflow.com/questions/1641182/how-can-i-catch-a-ctrl-c-event-c
*
*******************************************************************/
void SignalSetup()
{
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = ExitHandler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigaction(SIGINT, &sigIntHandler, NULL);
}

/*******************************************************************
*	Function: sigintHandler
*
*	Purpose: This function catches forced exits, and close the 
*			software
*
*	Inputs:	 	
*		1) int sig_num: this is the signal that was thrown
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void ExitHandler(int signal)
{
	system_up = 0;
	while(PT_display_up);	
	fflush(stdout);
	system("clear");
    	printf("CLOSING PROTOCOLS...\n");
	fclose(file_descriptor);
	exit(1);
}

/*******************************************************************
*	Function: SerialSetup
*
*	Purpose: This functions design is to setup the serial port
*			to allow for communication between the FCS
*			and the user terminal
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: Setup code referenced from: 
*	http://stackoverflow.com/questions/21019148/reading-from-serial-port-on-linux-and-osx
*
*******************************************************************/
void SerialSetup()
{
	uart_connection = open("/dev/ttyUSB0", O_RDONLY | O_NOCTTY);

	if (uart_connection == -1)
	{
		printf("FAILURE - /dev/ttyUSB0 failed to open...\n");
		system_up = 0;
	}

	else
	{
		printf("SUCCESS - /dev/ttyUSB0 successfully opened...\n");
		struct termios tty;
		struct termios tty_old;
		memset (&tty, 0, sizeof tty);

		/* Error Handling */
		if (tcgetattr(uart_connection, &tty) != 0) 
			printf("Error %d from tcgetattr: %s!\n", errno, strerror(errno));

		/* Save old tty parameters */
		tty_old = tty;

		/* Set Baud Rate */
		cfsetospeed (&tty, (speed_t)B115200);
		cfsetispeed (&tty, (speed_t)B115200);

		/* Setting other Port Stuff */
		tty.c_cflag     &=  ~PARENB;        // Make 8n1
		tty.c_cflag     &=  ~CSTOPB;
		tty.c_cflag     &=  ~CSIZE;
		tty.c_cflag     |=  CS8;

		tty.c_cflag     &=  ~CRTSCTS;       // no flow control
		tty.c_cc[VMIN]  =   1;
		tty.c_cc[VTIME] =   5;
		tty.c_cflag     |=  CREAD | CLOCAL; // turn on READ & ignore ctrl lines

		/* Make raw */
		cfmakeraw(&tty);

		/* Flush Port, then applies attributes */
		tcflush(uart_connection, TCIFLUSH);
		if ( tcsetattr ( uart_connection, TCSANOW, &tty ) != 0)
			printf("Error %d from tcgetattr: %s!\n", errno, strerror(errno));
	}
}

/*******************************************************************
*	Function: FileSetup
*
*	Purpose: This funuction sets up a data log file based on 
*			the curren time
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void FileSetup()
{
	time (&raw_time);
	time_info = localtime(&raw_time);

	char * file_name = (char * )malloc(strlen("/home/ory/Documents/School/FCS/DataLogs/") + strlen(asctime(time_info)) + strlen("txt"));
	strcpy(file_name, "/home/ory/Documents/School/FCS/DataLogs/");
	strcat(file_name, asctime(time_info));
	file_name[strlen(file_name) - 1] = '.';
	strcat(file_name, "txt");

	file_descriptor = fopen(file_name, "w+");

	if (file_descriptor == 0)
	{
		printf("ERROR - Data log could not be opened...\n");
		system_up = 0;
	}	

	else
		printf("SUCCESS - Data log opened...\n");
}

/*********************************************************************
*	Function: PushMessage
*
*	Purpose:  This function will add a new node to the linked list
*
*	Inputs:      
*					1) char * message: this is the message that will be added to 
*						the FCS message queue
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void PushMessage(char * message)
{
	//If list is empty
	if (m_head == NULL)
	{	
		m_head = (struct FCS_message *)malloc(sizeof(struct FCS_message));
		m_head->next = NULL;
		
		m_head->message = (char *)malloc(strlen(message) + 1);
		strcpy(m_head->message, message);
		m_tail = m_head;
		travel = m_head;
	}
	
	else
	{
		m_tail->next = (struct FCS_message *)malloc(sizeof(struct FCS_message));
		m_tail = m_tail->next;
		m_tail->next = NULL;
		m_tail->message = (char *)malloc(strlen(message) + 1);
		strcpy(m_tail->message, message);
	}	
	
	nodes_in_list++;

	if (nodes_in_list > 20)
		PopMessage();
}

/*********************************************************************
*	Function: PopMessage
*
*	Purpose:  This function will remove the oldest node in the list
*
*	Inputs:      
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void PopMessage()
{	struct FCS_message * temp = NULL;

	if (m_head != NULL)
	{
		if (m_head == m_tail)
			m_tail = m_head->next;
	
		temp = m_head;
		m_head = m_head->next;
	
		free(temp->message);
		free(temp);
		nodes_in_list--;
	}
}

/*******************************************************************
*	Function: CheckMessage
*
*	Purpose: This function will check the message for a key word
*			and respond accordingly if appropriate
*
*	Inputs:	 	
*		1) message: This is the key word that will be checked
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void CheckMessage(char * message)
{
	int value = 0;

	if (strcmp(strtok(message, ";"), "*") == 0)
	{
		for(value = 0; value < 7; value++)
			sensor_data[value] = atoi(strtok(NULL, ";"));
	}
}

/*******************************************************************
*	Function: PT_DisplayData
*
*	Purpose: This thread will display the data to the user
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void * PT_DisplayData(void * arg)
{
	PT_display_up = 1;
	int count = 13;
	
	fflush(stdout);
	system("clear");
	printf("===================================================================\n");
	printf("	SENSOR DATA		          |     FLIGHT STATUS	  |\n");
	printf("			 	          |			  |\n");
	printf("FRONT LEFT:          FRONT RIGHT:         |			  |\n");
	printf("				          | 			  |\n");
	printf("LEFT:                RIGHT:               |			  |\n");
	printf("			 	          |			  |\n");
	printf("REAR:                                     |			  |\n");
	printf("				          |			  |\n");
	printf("POSITIVE:            NEGATIVE:            |			  |\n");
	printf("			     	          |			  |\n");
	printf("===================================================================\n");
	printf("\033[13;0H");
	printf("System messages:\n");

	while(system_up && PT_display_up)
	{
		printf("\033[4;13H");
		printf("     ");
		printf("\033[4;13H");
		printf("%.1f", sensor_data[0]);

		printf("\033[6;13H");
		printf("     ");
		printf("\033[6;13H");
		printf("%.1f", sensor_data[1]);

		printf("\033[8;13H");
		printf("     ");
		printf("\033[8;13H");
		printf("%.1f", sensor_data[2]);

		printf("\033[10;13H");
		printf("     ");
		printf("\033[10;13H");
		printf("%.1f", sensor_data[3]);

		printf("\033[4;35H");
		printf("     ");
		printf("\033[4;35H");
		printf("%.1f", sensor_data[4]);

		printf("\033[6;35H");
		printf("     ");
		printf("\033[6;35H");
		printf("%.1f", sensor_data[5]);

		printf("\033[10;35H");
		printf("     ");
		printf("\033[10;35H");
		printf("%.1f", sensor_data[6]);

		if (new_message > 0 && m_head != NULL)
		{
			travel = m_head;

			for (count = 14; count < 33 && travel != NULL; count++)
			{
				fflush(stdout);
				printf("\033[%d;0H", count);
				printf("                                                                                "); //80 spaces
				printf("\033[%d;0H", count);
				printf("%s", travel->message);			
				travel = travel->next;
			}

			new_message = 0;
		}
	}

	PT_display_up = 0;
	fputs("TERMINAL: Display thread exiting...\n", file_descriptor);
	pthread_exit(NULL);
}
