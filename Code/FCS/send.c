#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>

void SignalSetup();
void ExitHandler(int signal);
void SerialSetup();
void CompileCommand(char * buffer, char * command);

void NORMAL();
void FAILURE();

int uart_connection		= -1;
int system_up 			= 1;

int main()
{
	system("clear");

	char buffer[10];
	char * buffer_1;
	size_t len = 64;
	char buffer_2[16];
	int i = 0;
	int index = 0;
	int writing = 0;
	char * token = NULL;

	buffer_1 = (char *)malloc(len * sizeof(char));
	memset(buffer_2, 0, sizeof(buffer_2));
	memset(buffer, 0, sizeof(buffer));

	if (system_up)
		SignalSetup();

	if (system_up)
		SerialSetup();

	if (system_up)
		printf("Ready to send commands:\n--------------------------------------\n");

	while(system_up)
	{
		buffer_1 = (char *)malloc(len * sizeof(char));

		printf("Enter your command: ");
		fflush(stdout);
		getline(&buffer_1, &len, stdin);
		buffer_1[strlen(buffer_1) - 1] = '\0';
		
		if (strcmp(buffer_1, "exit") == 0)
		{
			system("clear");
			printf("Shutting system down...\n");
			system_up = 0;
		}
		
		else if (strcmp(strtok(buffer_1, " "), "TEST") == 0)
		{
			token = strtok(buffer_1, " ");
			
			if (strcmp(token, "NORMAL") == 0)
			{
				printf("Initiating NORMAL test.../n");
				
				NORMAL();
				
				printf("NORMAL test complete...\n");
			}
			
			else if (strcmp(token, "FAILURE") == 0)
			{
				printf("Initiating FAILURE test...\n");
				
				FAILURE();
				
				printf("FAILURE test complete.../n");
			}
			
			else
				printf("Invalid test parameter.../n");
		}
		
		else
		{
			printf("Is this the command you wish to send [Y/n]: %s\nENTRY:", buffer_1);
			scanf("%s", buffer_2);

			if ((strcmp(buffer_2, "Y") == 0) || (strcmp(buffer_2, "y") == 0))
			{
				printf("Sending...\n\n");
				writing = 1;
				index = 0;
				
				while (writing)
				{
					memset(buffer, 0, sizeof(buffer));

					for (i = 0; i < 6 && writing; i++, index++)
					{
						if (buffer_1[index] != '\0')
							buffer[i] = buffer_1[index];

						else
							writing = 0;
					}

					if (!writing)
					{
						buffer[strlen(buffer)] = '*';
						buffer[strlen(buffer) + 1] = '\0';
					}

					write(uart_connection, buffer, strlen(buffer));
					usleep(5000);
				}
			}

			else
				printf("Discarding...\n\n");

			while(getchar() != '\n');
		}

		free(buffer_1);
		memset(buffer_2, 0, sizeof(buffer_2));
		memset(buffer, 0, sizeof(buffer));
	}

	return 0;
}

/*******************************************************************
*	Function: SignalSetup
*
*	Purpose: This function sets up the forced exit signal
*
*	Inputs:		N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		Setup Provided by:
* http://stackoverflow.com/questions/1641182/how-can-i-catch-a-ctrl-c-event-c
*
*******************************************************************/
void SignalSetup()
{
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = ExitHandler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigaction(SIGINT, &sigIntHandler, NULL);
}

/*******************************************************************
*	Function: sigintHandler
*
*	Purpose: This function catches forced exits, and close the 
*			software
*
*	Inputs:	 	
*		1) int sig_num: this is the signal that was thrown
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void ExitHandler(int signal)
{
	system_up = 0;	
	fflush(stdout);
	system("clear");
    	printf("CLOSING PROTOCOLS...\n");
	exit(1);
}

/*******************************************************************
*	Function: SerialSetup
*
*	Purpose: This functions design is to setup the serial port
*			to allow for communication between the FCS
*			and the user terminal
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: Setup code referenced from: 
*	http://stackoverflow.com/questions/21019148/reading-from-serial-port-on-linux-and-osx
*
*******************************************************************/
void SerialSetup()
{
	uart_connection = open("/dev/ttyUSB0", O_WRONLY | O_NOCTTY);

	if (uart_connection == -1)
	{
		printf("FAILURE - /dev/ttyUSB0 failed to open...\n");
		system_up = 0;
	}

	else
	{
		printf("SUCCESS - /dev/ttyUSB0 successfully opened...\n");
		struct termios tty;
		struct termios tty_old;
		memset (&tty, 0, sizeof tty);

		/* Error Handling */
		if (tcgetattr(uart_connection, &tty) != 0) 
			printf("Error %d from tcgetattr: %s!\n", errno, strerror(errno));

		/* Save old tty parameters */
		tty_old = tty;

		/* Set Baud Rate */
		cfsetospeed (&tty, (speed_t)B115200);
		cfsetispeed (&tty, (speed_t)B115200);

		/* Setting other Port Stuff */
		tty.c_cflag     &=  ~PARENB;        // Make 8n1
		tty.c_cflag     &=  ~CSTOPB;
		tty.c_cflag     &=  ~CSIZE;
		tty.c_cflag     |=  CS8;

		tty.c_cflag     &=  ~CRTSCTS;       // no flow control
		tty.c_cc[VMIN]  =   1;
		tty.c_cc[VTIME] =   5;
		tty.c_cflag     |=  CREAD | CLOCAL; // turn on READ & ignore ctrl lines

		/* Make raw */
		cfmakeraw(&tty);

		/* Flush Port, then applies attributes */
		tcflush(uart_connection, TCIFLUSH);
		if ( tcsetattr ( uart_connection, TCSANOW, &tty ) != 0)
			printf("Error %d from tcgetattr: %s!\n", errno, strerror(errno));
	}
}

/*******************************************************************
*	Function: CompileCommand
*
*	Purpose: This function will compile a command line to send to the 
*			the FCS to be executed as a simulation
*
*	Inputs:	 	
*				1) char * buffer
*					This character buffer holds the priorities that 
*					will need to be compiled into a command
*
*				2) char * command
*					This character buffer will hold the compiled command
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void CompileCommand(char * buffer, char * command)
{
	char holder[15] = { '\0' };
	int count = 0;
	int buffer_count = 0;
	
	for (count = 0; count < 13; count++)
	{
		if (count % 2)
		{
			holder[count] = buffer[buffer_count];
			buffer_count++;
		}
		
		else
			holder[count] = ' ';
	}
	
	memset(command, '\0', sizeof(command));
	strcpy(command, "DATA ");
	strcat(command, holder);
}

/*******************************************************************
*	Function: NORMAL
*
*	Purpose: This function will execute a normal takeoff simulation 
*			for the FCS
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*******************************************************************/
void NORMAL()
{
		char priority[7] = { '\0' };
		char command[25] = { '\0' };
		
		//TAKEOFF
		
		//Setup a normal takeoff condition
		priority[0] = 2;
		priority[1] = 2;
		priority[2] = 2;
		priority[3] = 1;
		priority[4] = 2;
		priority[5] = 2;
		priority[6] = 6;
		
		//Compile first batch of data and send over, wait 1 second
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(1000000);
		
		//Send command to lift -> TakeOff, wait 3 seconds
		strcpy(command, "TAKEOFF");
		write(uart_connection, buffer, strlen(buffer));
		usleep(3000000);
		
		//Send command to lift -> maintain
		priority[0] = 2;
		priority[1] = 2;
		priority[2] = 2;
		priority[3] = 1;
		priority[4] = 2;
		priority[5] = 2;
		priority[6] = 4;
		
		//Compile second batch of data and send over, wait 3 seconds
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(3000000);
		
		//Send command to maintain -> complete
		priority[0] = 2;
		priority[1] = 2;
		priority[2] = 2;
		priority[3] = 1;
		priority[4] = 2;
		priority[5] = 2;
		priority[6] = 3;
		
		//Compile command to send to complete the TakeOff sequence
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(3000000);
		
		//HOVER
		
		//Initiate a slow approach from the port side
		priority[1] = 3;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(2000000);
		
		//Initiate a slow approach from astern
		priority[2] = 3;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(2000000);
		
		//Remove port pressure
		priority[1] = 2;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(2000000);
		
		//Initiate a slow approach from starboard side
		priority[5] = 3;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(2000000);
		
		//Remove astern pressure
		priority[2] = 2;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(2000000);
		
		//Remove starboard pressure
		priority[5] = 2;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(2000000);
		
		//LAND
		
		//Initiate LAND, allow descent
		strcpy(command, "LAND");
		write(uart_connection, command, strlen(command));
		usleep(1000000);
		
		//Change from descent -> berthing
		priority[6] = 4;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(3000000);
		
		//Change from berthing -> berth
		priority[6] = 6;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(3000000);
		
		//Simulation complete
}

/*******************************************************************
*	Function: FAILURE
*
*	Purpose: This function will execute a failure takeoff simulation
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*******************************************************************/
void FAILURE()
{
		char priority[7] = { '\0' };
		char command[25] = { '\0' };
		
		//TAKEOFF
		
		//Setup a normal takeoff condition
		priority[0] = 2;
		priority[1] = 2;
		priority[2] = 2;
		priority[3] = 1;
		priority[4] = 2;
		priority[5] = 2;
		priority[6] = 6;
		
		//Compile first batch of data and send over, wait 1 second
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(1000000);
		
		//Send command to lift -> TakeOff, wait 3 seconds
		strcpy(command, "TAKEOFF");
		write(uart_connection, buffer, strlen(buffer));
		usleep(3000000);
		
		//Send command to lift -> e_land
		priority[0] = 2;
		priority[1] = 2;
		priority[2] = 2;
		priority[3] = 5;
		priority[4] = 2;
		priority[5] = 2;
		priority[6] = 4;
		
		//Compile second batch of data and send over, wait 3 seconds
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(1000000);	
		
		priority[6] = 5;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(1000000);
		
		priority[6] = 5;
		CompileCommand(priority, command);
		write(uart_connection, command, strlen(command));
		usleep(1000000);		
}