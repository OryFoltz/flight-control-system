/********************************************************************
*	File: 	main.c
*	Author:	Ory Foltz
*	Date:	1/8/17
*
*	Purpose: This file is the housing for the entire FCS program. 
*
*	Functions:
*		1) SPISetup()
*
********************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <wiringPiSPI.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <softPwm.h>
#include <wiringPi.h>
#include <unistd.h>

//Data Interfacing
void VariableSetup();
void SignalSetup();
void ExitHandler(int signal);
void SPISetup();
int WriteSPICHAR(char * data);
void DataLoading();
void PriorityComputation();
void StandBy();
void TakeOff();
void Hover();
void Land();
void ELand();
void Critical();
void MessageQueue(char * message);
void StartTimer(void);
void StopTimer(void);
void ThreadCheck(int sig);
void UARTSetup();
void SendUART(char * data);
void ThreadSetup();
void PushMessage(char * message);
void PopMessage();
void FileSetup();
void PWMSetup();
void Arm();
void Disarm();
void SetPWM(int pin, int duty_cycle);
void CommandHandler(char * command);
char * SensorDataString(char * sensor_string);
int FindDirection(int axis);
void MotorControl(int direction);
void EXE_StandBy();
void EXE_TakeOff();
void EXE_Maneuver();
void EXE_Land();
void EXE_ELand();
void EXE_Critical();

//User Interfacing
void * PT_DisplayData(void * arg);

//Distance Calculations
void * PT_CalculateDistance(void * arg);

//Receiving data from the user terminal
void * PT_ReceiveData(void * arg);

//Sending Messages to the user terminal
void * PT_SendMessage(void * arg);

//This is the thread that issues commands through PWM to the flight controller
void * PT_Pilot(void * arg);

//This thread sends the sensor values to the ground control terminal
void * PT_SensorData(void * arg);

//This thread will be used for software simulation
void * PT_TestThread(void * arg);

//The flight "modes" of the FCS
enum flight_command 
{
	standby		= 0,
	takeoff 	= 1,
	hover 		= 2,
	flight 		= 3,
	land		= 4,
	e_land 		= 5,
	critical	= 6,
};

//The priority of sensors from objects
enum evasion_priority
{
	zero	= 0,	//>= 15 feet
	one		= 1,	//>= 10 feet
	two 	= 2,	//>= 8 feet
	three 	= 3, 	//>= 5 feet
	four	= 4,	//>= 2 feet
	five	= 5,	//>= 1.5 feet
	six		= 6, 	//>= 0 feet
};

//The value tied to a specific sensor
enum sensor_id
{
	front_l	= 0, //Front Left sensor
	left	= 1, //Left Sensor
	rear	= 2, //Rear Sensor
	posi	= 3, //Positive (+Z) sensor
	front_r	= 4, //Front Right sensor
	right	= 5, //Right sensor
	nega	= 6, //Negative (-Z) sensor
};

//A takeoff sequence the FCS executes
enum takeoff_sequence
{
	startup		= 0,
	check		= 1,
	initiate 	= 2,
	lift 		= 3,
	maintain	= 4,
	complete	= 5,
};

//A landing sequence the FCS executes
enum landing_sequence
{
	initializing 	= 0,
	confirm 		= 1,
	descent			= 2,
	berthing		= 3,
	berth			= 4,
};

//Used for PWM channels, number corresponds to pin
enum PWM_channels
{
	yaw	        = 25,
	pitch	    = 27,
	throttle    = 22,
	roll	    = 6,
	arm	        = 21,
};

/*************************************************
*	Note: 
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*************************************************/
struct flight_control	
{
	enum flight_command 	command;
	enum evasion_priority	priority[7];
    
	int	ahead;
	int	astern;
	int	starboard;
	int	port;
	int	positive;
	int negative;
	int stationary;
};

//Struct for the linked list
struct FCS_message
{
	char * message;
	struct FCS_message * next;
};

//Global variable data
pthread_t calculate_data;
pthread_t receive_data;
pthread_t send_message;
pthread_t pilot;
pthread_t send_sensor;
pthread_t test;

struct flight_control FCS_data;

timer_t thread_monitor;

float sensor_data[7];
int SPI_buffer[7];
int spi_connection;
int uart_connection;

int system_up;
int hold;
int in_test;
int PT_calculate_up;
int PT_receive_up;
int PT_send_up;
int PT_pilot_up;
int PT_sensor_up;
int PT_test_up;

int PT_calculate_hit;
int PT_send_hit;
int PT_pilot_hit;
int PT_sensor_hit;
int PT_test_hit;

int new_message;
int nodes_in_list;
int armed;
int disarmed;
int critical_set;
char * flight_status[10];
struct FCS_message * m_head;
struct FCS_message * m_tail;

FILE * file_descriptor = NULL;
time_t raw_time;
struct tm * time_info;

//Global Static data
static const int SPI_CHANNEL = 1;

/********************************************************************
*	Function:  Main
*
*	Purpose:   This function will be the "housing" for 
*		the program. It will link all the FCS 
*		functions together, and run the main
*		execution loops for the program.
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables:
*		1) run_program: This variable will be used 
*			in a while loop and will determine 
*			when the software is to hault
*		2) thread_error: This will be one when the creation
*			of the display thread has failed
*
*	Bugs:      N/A
*
*	Notes:     N/A
*
********************************************************************/
int main()
{	
	wiringPiSetup();	

	//Start systems
	VariableSetup();
	delay(2);
	FileSetup();
	delay(2);
	SPISetup();
	delay(2);
	UARTSetup();
	delay(2);
	SignalSetup();
	delay(2);
	ThreadSetup();
	PWMSetup();

	//Wait for threads to initialize
	while (!PT_send_up ||  !PT_calculate_up || !PT_receive_up || !PT_pilot_up || !PT_test_hit);
	
	//Waiting for the test thread to continue
	while (hold);
	
	fputs("SYSTEM: FCS booting...\n", file_descriptor);
	PushMessage("SYSTEM: FCS booting...\n");	

	//Start thread monitor
	(void) signal(SIGALRM, ThreadCheck);
	StartTimer();

	int count = 0;
	char test_buffer[50];
	
	//This is the main execution loop that will run the program
	while (system_up)
	{		
		switch (FCS_data.command)
		{
			case standby:
				StandBy();
			break;

			case takeoff:
				TakeOff();
			break;

			case hover:
				Hover();
			break;

			case flight:
			break;

			case land:
				Land();
			break;

			case e_land:
				ELand();
			break;

			case critical:
				Critical();
				break;

			default:
			break;
		}
	};

	fclose(file_descriptor);
	return 0;
}

/*******************************************************************
*	Function: VariableSetup
*
*	Purpose: This function sets up the system variables
*
*	Inputs:			N/A
*
*	Outputs: 		N/A
*
*	Variables: 	N/A
*
*	Bugs:			N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void VariableSetup()
{
	system_up 		    = 1;
	hold 				= 1;
	in_test				= 0;
	new_message 		= 0;
	nodes_in_list 		= 0;
	disarmed 		    = 0;
	armed			    = 0;
    critical_set        = 0;
	PT_calculate_up		= 0;
	PT_send_up 		    = 0;
	PT_receive_up		= 0;
	PT_pilot_up 		= 0;
	PT_sensor_up		= 0;
    PT_test_up          = 0;
	PT_calculate_hit	= 0;
	PT_send_hit		    = 0;
	PT_pilot_hit		= 0;
	PT_sensor_hit		= 0;
    PT_test_hit         = 0;

	memset(flight_status, 0, sizeof(flight_status));
	
	FCS_data.command 	= standby;
	FCS_data.ahead 	= 0;
	FCS_data.astern 	= 0;
	FCS_data.starboard 	= 0;
	FCS_data.port 		= 0;
	FCS_data.positive 	= 0;
	FCS_data.negative 	= 0;
	
	m_head 	= NULL;
	m_tail 	= NULL;
}

/*******************************************************************
*	Function: SignalSetup
*
*	Purpose: This function sets up the forced exit signal
*
*	Inputs:		N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		Setup Provided by:
* http://stackoverflow.com/questions/1641182/how-can-i-catch-a-ctrl-c-event-c
*
*******************************************************************/
void SignalSetup()
{
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = ExitHandler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigaction(SIGINT, &sigIntHandler, NULL);
	
	fputs("SUCCESS - Signal Handler running...\n", file_descriptor);
	PushMessage("SUCCESS - Signal Handler running...\n");
}

/*******************************************************************
*	Function: ExitHandler
*
*	Purpose: This function catches forced exits, and close the 
*			software
*
*	Inputs:	 	
*		1) int sig_num: this is the signal that was thrown
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void ExitHandler(int signal)
{
	fputs("SYSTEM: Shutting down system\n", file_descriptor);
	PushMessage("SYSTEM: Shutting down system... Please reply with OK\n");
	system_up = 0;
	while(PT_receive_up  || PT_calculate_up || PT_sensor_up || PT_pilot_up || PT_test_up);
	while(nodes_in_list);
	PT_send_up = 0;
	delay(10);
	fclose(file_descriptor);
	exit(1);
}


/*********************************************************************
*	Function: SPISetup
*
*	Purpose:  This function initiates the SPI on the RPI 0
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void SPISetup()
{
	//Ensure the SPI fucntion initialized 
	fputs("SUCCESS - Initializing SPI...\n", file_descriptor);
	PushMessage("SUCCESS - Initializing SPI...\n");

	//This gets the FD for the connection
	spi_connection = wiringPiSPISetup(SPI_CHANNEL, 1000000);
}

/********************************************************************
*	Function:  int WriteSPICHAR(char * data)
*
*	Purpose:   This function will write to the SPI and then 
*			return data from the SPI
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables:
*		1) run_program: This variable will be used 
*			in a while loop and will determine 
*			when the software is to hault
*		2) thread_error: This will be one when the creation
*			of the display thread has failed
*
*	Bugs:      N/A
*
*	Notes:     N/A
*
********************************************************************/
int WriteSPICHAR(char * data)
{
	int result = 0;
	float rx_data = 0;
	int catch_data = 0;
	unsigned char rx_buffer[256];

	strcpy(rx_buffer, data);

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("ERROR - Write to SPI with data %s failed...\n", rx_buffer);

	catch_data = (int)rx_buffer[0];

	return catch_data;
}

/*********************************************************************
*	Function: DataLoading()
*
*	Purpose:  This thread will pull data from the sensors then 
*			return it
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void DataLoading()
{
	int result = 0;
	unsigned char rx_buffer[256];
	char data_request;

	data_request = data_request & 0b00000000;
	data_request = data_request | 0b00010000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	//Trash read to queue up sensor one
	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	//Retrieve data from sensor 1, fetch data from sensor 2
	data_request = data_request & 0b00000000;
	data_request = data_request | 0b00100000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	SPI_buffer[0] = (int)rx_buffer[0];

	//Retrieve data from sensor 2, fetch data from sensor 3
	data_request = data_request & 0b00000000;
	data_request = data_request | 0b00110000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	SPI_buffer[1] = (int)rx_buffer[0];

	//Retrieve data from sensor 3, fetch data from sensor 4
	data_request = data_request & 0b00000000;
	data_request = data_request | 0b01000000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	SPI_buffer[2] = (int)rx_buffer[0];

	//Retrieve data from sensor 4, fetch data from sensor 5
	data_request = data_request & 0b00000000;
	data_request = data_request | 0b01010000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	SPI_buffer[3] = (int)rx_buffer[0];
	
	//Retrieve data from sensor 5, fetch data from sensor 6
	data_request = data_request & 0b00000000;
	data_request = data_request | 0b01100000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	SPI_buffer[4] = (int)rx_buffer[0];
	
	//Retrieve data from sensor 6, fetch data from sensor 7
	data_request = data_request & 0b00000000;
	data_request = data_request | 0b01110000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	SPI_buffer[5] = (int)rx_buffer[0];

	//Retrieve data from sensor 7, write trash
	data_request = data_request & 0b00000000;
	data_request = data_request | 0b00010000;

	memset(rx_buffer, '\0', sizeof(rx_buffer));
	rx_buffer[0] = data_request;

	result = wiringPiSPIDataRW(spi_connection, rx_buffer, strlen(rx_buffer));

	if (spi_connection == -1)
		printf("Error - Write failed!\n");

	SPI_buffer[6] = (int)rx_buffer[0];
}

/*********************************************************************
*	Function: PriorityComputation
*
*	Purpose:  This function will compute the priorities based on the 
*			inputs
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   
*		[0] = front left sensor
*		[1] = left sensor
*		[2] = rear sensor
*		[3] = positive sensor (+z axis)
*		[4] = front right sensor
*		[5] = right sensor
*		[6] = negative sensor (-z axis)
*
********************************************************************/
void PriorityComputation()
{
	int count = 0;

	for (count = 0; count < 7 && !in_test; count++)
	{
		if (sensor_data[count] > 180)
			FCS_data.priority[count] = zero;

		else if (sensor_data[count] > 120)
			FCS_data.priority[count] = one;

		else if (sensor_data[count] > 96)
			FCS_data.priority[count] = two;

		else if (sensor_data[count] > 60)
			FCS_data.priority[count] = three;

		else if (sensor_data[count] > 24)
			FCS_data.priority[count] = four;
		
		else if (sensor_data[count] > 18)
			FCS_data.priority[count] = five;

		else
			FCS_data.priority[count] = six;
	}
}

/*********************************************************************
*	Function: StandBy
*
*	Purpose:  This function will execute the standby mode for the FCS
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void StandBy()
{
	Disarm();
	FCS_data.command 	    = standby;
	FCS_data.ahead 	    	= 0;
	FCS_data.astern 	    = 0;
	FCS_data.starboard 	    = 0;
	FCS_data.port 		    = 0;
	FCS_data.positive 		= 0;
	FCS_data.negative 		= 0;
}

/*********************************************************************
*	Function: TakeOff
*
*	Purpose:  This function will execute the takeoff command if viable
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void TakeOff()
{
	int executing = 0;
	int count = 0;
	int flag = 0;
	enum takeoff_sequence takeoff_commands = startup;

	executing = 1;

	while(executing)
	{
		//Switch based on the current takeoff command attempting to be executed
		switch (takeoff_commands)
		{
			//This case statement will let the systems know a takeoff is initiating
			case startup:
			
				fputs("TAKEOFF: Initialized...\n", file_descriptor);
				PushMessage("TAKEOFF: Initializing...\n");
				takeoff_commands = check; 
			break;

			//This statement will check the value of the surrounding area to make sure a takoff is 
			//within the tolerances
			case check:	
				
				fputs("TAKEOFF: Checking distances...\n", file_descriptor);
				PushMessage("TAKEOFF: Checking distances...\n");
				//Iterate through the sensors and make sure they meet the threshold 
				//for takeoff: Do not iterate through the negative sensor [6]
				for (count = 0, flag = 0; count < 6 && !flag; count++)
				{
					//If not the vertical sensor
					if (count != 3)
					{	
						//If item is closer then 2 feet (priority 5)
						if(FCS_data.priority[count] > four)
							flag = 1;
					}

					//else vertical sensor
					else
					{
						//If item is closer then 10 feet
						if (FCS_data.priority[count] > one)
							flag = 1;
					}
				}

				//If clear
				if (!flag)
				{
					fputs("TAKEOFF: Distance checking successful...\n", file_descriptor);
					PushMessage("TAKEOFF: Distance checking successful...\n");
					takeoff_commands = initiate;
				}

				//If not clear
				else
				{
					fputs("TAKEOFF: Distance checking failed.. Returning to STANDBY\n", file_descriptor);
					PushMessage("TAKEOFF: Distance checking failed... Returning to STANDBY\n");
					FCS_data.command = standby;
					executing = 0;
				}
				
				flag = 0;
			break;

			//Begin takeoff
			case initiate:
				
				//Turn on motors
				fputs("TAKEOFF: Activating motors...\n", file_descriptor);
				PushMessage("TAKEOFF: Activating motors...\n");
				
				FCS_data.ahead 				= 0;
				FCS_data.astern 			= 0;
				FCS_data.starboard 			= 0;
				FCS_data.port 				= 0;
				FCS_data.positive 			= 1;
				FCS_data.negative 			= 0;
				
				takeoff_commands = lift;
			break;

			//Monitor the first 2 feet of takeoff
			case lift:
			
				//Iterate through all of the sensors
				for (count = 0; count < 7; count++)
				{
					//If positive z axis
					if (count == 3)
					{
						//If closer then 2 feet
						if (FCS_data.priority[count] > four)
						{
							fputs("TAKEOFF: Phase 1 of takeoff -positive- failure.... Initiating E_LAND\n", file_descriptor);
							PushMessage("TAKEOFF: Phase 1 of takeoff -positive- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
						}
					}
					
					//If negative z axis
					else if (count == 6)
					{
						//If two feet off of the ground, complete stage, move to maintain
						if (FCS_data.priority[count] == four)
						{
                           	fputs("TAKEOFF: Phase 1 of takeoff complete...\n", file_descriptor);
							PushMessage("TAKEOFF: Phase 1 of takeoff complete...\n");
							takeoff_commands = maintain;
						}
					}
					
					//If any x/y axis
					else
					{
						//If object ever gets closer then 1.5 feet, go to emergency landing mode
						if (FCS_data.priority[count] = 6)
						{
                           	fputs("TAKEOFF: Phase 1 of takeoff -horizontal- failure... Initiating E_LAND\n", file_descriptor);
							PushMessage("TAKEOFF: Phase 1 of takeoff -horizontal- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
						}
					}
				}
	
			break;

			//Monitor takeoff from 2 -> 5 feet
			case maintain:
			
				//Iterate through all of the sensors
				for (count = 0; count < 7; count++)
				{
					//If positive z axis
					if (count == 3)
					{
						//If closer then 2 feet
						if (FCS_data.priority[count] > four)
						{
                            fputs("TAKEOFF: Phase 2 of takeoff -positive- failure... Initiating E_LAND\n", file_descriptor);
							PushMessage("TAKEOFF: Phase 2 of takeoff -positive- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
						}
					}
					
					//If negative z axis
					else if (count == 6)
					{
						//If five feet off of the ground, complete stage, move to complete
						if (FCS_data.priority[count] == three)
						{
                            fputs("TAKEOFF: Phase 2 of takeoff complete...\n", file_descriptor);
							PushMessage("TAKEOFF: Phase 2 of takeoff complete...\n");
							takeoff_commands = complete;
						}
					}
					
					//If any x/y axis
					else
					{
						//If object ever gets closer then 5 feet, go to emergency landing mode
						if (FCS_data.priority[count] > three)
						{
                            fputs("TAKEOFF: Phase 1 of takeoff -horizontal- failure... Initiating E_LAND\n", file_descriptor);
							PushMessage("TAKEOFF: Phase 1 of takeoff -horizontal- failure... Initiating E_LAND\n");
							FCS_data.command = e_land;
							executing = 0;
						}
					}
				}
			break;

			case complete:
			
				//Bring motors to hover
                fputs("TAKEOFF: Takeoff successful... Initiating HOVER\n", file_descriptor);
				PushMessage("TAKEOFF: Takeoff successful... Initiating HOVER\n");
				FCS_data.command = hover;
				FCS_data.ahead 				= 0;
				FCS_data.astern 			= 0;
				FCS_data.starboard 			= 0;
				FCS_data.port 				= 0;
				FCS_data.positive 			= 0;
				FCS_data.negative 			= 0;
				executing 					= 0;
			break;

			default:
			
				//Critical error
                fputs("TAKEOFF: Takeoff successful... Initiating HOVER\n", file_descriptor);
				PushMessage("TAKEOFF: Critical error... Initiating E_LAND\n");
				FCS_data.command = e_land;
				executing = 0;
			break;
		}		
	}
}

/*********************************************************************
*	Function: Hover
*
*	Purpose:  This function will execute the hover command
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void Hover()
{
	int count = 0;
	int l_lock = 0;
	int r_lock = 0;
	
	//This will iterate through all of sensors and check their
	//priorities and act accordingly
	for (count = 0; count < 7; count++)
	{
		
		//Switch based off of what sensor we are looking atan
		switch (count)
		{
			//Front Left Sensor
			case 0:
				
				//If sensor detects something closer then 8 feet and astern hasn't been set already
				if (FCS_data.priority[count] > two && FCS_data.astern == 0)
				{
					FCS_data.astern = 1;	
					l_lock = 1;
				}					
				
				//If not set by the othher forward sensor
				else if (!r_lock)
				{
					FCS_data.astern = 0;	
					l_lock = 0;
				}					
			break;
			
			//Left Sensor
			case 1:
			
				//If sensor detects something closer then 8 feet
				if (FCS_data.priority[count] > two)
					FCS_data.starboard = 1;
				
				else
					FCS_data.starboard = 0;
			break;
			
			//Rear Sensor
			case 2:
			
				//If sensor detects something closer then 8 feet
				if(FCS_data.priority[count] > two)
					FCS_data.ahead = 1;
				
				else
					FCS_data.ahead = 0;
			break;
			
			//Positive Sensor
			case 3: 
			
				//If the positive sensor detects something closer then 8 feet 
				if (FCS_data.priority[count] > two)
					FCS_data.negative = 1;
				
				else	
					FCS_data.negative = 0;
			break;
			
			//Front Right Sensor
			case 4:
			
				//If sensor detects object closer then 8 feet and stern hasnt been set
				if (FCS_data.priority[count] > two && FCS_data.astern == 0)
				{
					FCS_data.astern = 1;
					r_lock = 1;
				}
				
				//If the other forward sensor hasnt set it
				else if (!l_lock)
				{
					FCS_data.astern = 0;
					r_lock = 0;
				}
			break;
			
			//Right Sensor
			case 5:
			
				//If the right sensor detects something closer then 8 feet
				if (FCS_data.priority[count] > two)
					FCS_data.port = 1;
				
				else
					FCS_data.port = 0;
			break;
			
			//Negative Sensor
			case 6:
			
				//If the negative sensor detects something closer then 8 feet
				if (FCS_data.priority[count] > two)
					FCS_data.positive = 1;
				
				else
					FCS_data.positive = 0;
			break;

			default:
				//Critical error
				fputs("HOVER: Critical error... Initiating E_LAND\n", file_descriptor);
				PushMessage("HOVER: Critical error... Initiating E_LAND\n");
				FCS_data.command = e_land;
				count = 7;
			break;				
		}
	}
}

/*********************************************************************
*	Function: Land
*
*	Purpose:  This function will execute the Land command
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
**********************************************************************/
void Land()
{
	enum landing_sequence landing = initializing;
	int executing = 1;
	int count = 0;
	
	while (executing)
	{
		switch (landing)
		{
			case initializing:
				
				//Signal start of landing protocols
				fputs("LAND: Initializing...\n", file_descriptor);
				PushMessage("LAND: Initializing...\n");
				landing = confirm;
			break;
			
			case confirm:
			
				//Checking if ground is farther then 5
				if (FCS_data.priority[nega] <= three)
				{
					landing = descent;
					fputs("LAND: Beginning descent...\n", file_descriptor);
					PushMessage("LAND: Beginning descent...\n");
				}
				
				//If ground is closer then 5 feet
				else
				{
					landing = berthing;
					fputs("LAND: Berthing...\n", file_descriptor);
					PushMessage("LAND: Berthing...\n");
				}
				
			break;
			
			case descent:
				
				FCS_data.negative = 1;
				
				while ((landing == descent) && executing)
				{    

				    //If ground is closer then 5 feet
				    if (FCS_data.priority[nega] > 3)
					landing = berthing;
				
				    //Check priorities of other sensors while landing
				    for ( count = 0; count < 6; count++)
				    {
					//If anything is closer then 2 feet
					if (FCS_data.priority[count] >= four)
					{
					    fputs("LAND: Descent interference... Initiating E_LAND\n", file_descriptor);
					    PushMessage("LAND: Descent interference... Initiating E_LAND\n");
					    executing = 0;
					    FCS_data.command = e_land;
					}
				    }
				}
					
			break;
			
			case berthing:
			
				FCS_data.negative = 1;
				
				while((landing == berthing) && executing)
				{
	    
				    //If ground is closer then two feet, narrow peripheral checks
				    if (FCS_data.priority[nega] > four)
				    {
					//If anything is closer then 2 feet above it, initiate emergency landing
					if (FCS_data.priority[posi] > four)
					{
					    fputs("LAND: Berthing interference... Initiating E_land\n", file_descriptor);
					    PushMessage("LAND: Berthing interference... Initiating E_land\n");
					    executing = 0;
					    FCS_data.command = e_land;
					}
					
					//If the ground is close enough to complete the landing
					if (FCS_data.priority[nega] >= six)
					    landing = berth;
				    }
				
				    //Else maintain same checks as in descent
				    else
				    {
					for ( count = 0; count < 6; count++)
					{
					    //If anything is closer then 2 feet
				    	    if (FCS_data.priority[count] > four)
					    {
							fputs("LAND: Berthing interference... Initiating E_LAND\n", file_descriptor);
						    PushMessage("LAND: Berthing interference... Initiating E_LAND\n");
						    executing = 0;
						    FCS_data.command = e_land;
					    }
					}	
				    }
				}
			
			break;
			
			case berth:
			
				fputs("LAND: Landing complete... Initializing STANDBY\n", file_descriptor);
				PushMessage("LAND: Landing complete... Initializing STANDBY\n");
				
				FCS_data.negative = 0;
				FCS_data.command = standby;
				executing = 0;
			break;
			
			default:
            
				//Critical error
				fputs("LAND: Critical error... Initiating E_LAND\n", file_descriptor);
				PushMessage("LAND: Critical error... Initiating E_LAND\n");
				FCS_data.command = e_land;
				executing = 0;
			break;
		}
	}
}

/*********************************************************************
*	Function: ELand
*
*	Purpose:  This function will execute the ELand command. In this
*				lockout state the FCS is only concerned with landing
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
**********************************************************************/
void ELand()
{
	
	PushMessage("E_LAND: Emergency Land intiated...\n");
	//While the ground is 1.5 feet away
	while (FCS_data.priority[6] < six)
	{
		//Iniate a controlled fall to the ground
		FCS_data.negative 	= 1;
		FCS_data.ahead 		= 0;
		FCS_data.astern 	= 0;
		FCS_data.starboard 	= 0;
		FCS_data.port 		= 0;
		FCS_data.positive 	= 0;
	}
	
	FCS_data.negative 	= 0;
	
    fputs("E_LAND: Emergency Land completed... Initiating STANDBY\n", file_descriptor);
	PushMessage("E_LAND: Emergency Land completed... Initiating STANDBY\n");
	FCS_data.command = standby;
}

/*********************************************************************
*	Function: Critical
*
*	Purpose:  This function will only be entered if a critical system 
*				error has occured and the FCS is no longer functional.
*				The Critical mode will hold the program, ending threads 
*				and tasks that wre not needed. The FCS must be hard reset
*				after entering the Critical state. 
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
**********************************************************************/
void Critical()
{
	int count = 0;
	
	//Message the system a critical error has occured
	fputs("CRITICAL: A critical system error hads occurred... Holding in CRITICAL\n", file_descriptor);
	PushMessage("CRITICAL: A critical system error has occured... Holding in CRITICAL\n");	
	fclose(file_descriptor);

	//End critical functioning threads
	PT_calculate_up = 0;
	PT_receive_up 	= 0;
	PT_send_up 		= 0;
	
	//Indefinetely clear values
	while (1)
	{
        critical_set = 1;
        
		Disarm();
		FCS_data.negative 	= 0;
		FCS_data.ahead 	    = 0;
		FCS_data.astern 	= 0;
		FCS_data.starboard 	= 0;
		FCS_data.port 		= 0;
		FCS_data.positive 	= 0;
		
		for (count = 0; count < 7; count++)
			FCS_data.priority[count] = zero;
		
		FCS_data.command = critical;
	}
}

/*********************************************************************
*	Function: MessageQueue
*
*	Purpose:  This function will queue up messages to be displayed
*
*	Inputs:    
*				1) char * message: this will contain the new messge 
*					that needs to be queued up for display
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void MessageQueue(char * message)
{
	new_message++;
	
	PushMessage(message);
	
	//If the amount of nodes exceeds what we allow
	if (nodes_in_list > 9)
		PopMessage();		
}

/*********************************************************************
*	Function: StartTimer
*
*	Purpose:  This function simply starts the timer at a 1 second 
*				interval
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   Cited to newbie14 at www.daniweb.com
*
********************************************************************/
void StartTimer(void)
{
	struct itimerspec value;

	value.it_value.tv_sec = 1;
	value.it_value.tv_nsec = 0;

	value.it_interval.tv_sec = 1;
	value.it_interval.tv_nsec = 0;

	timer_create (CLOCK_REALTIME, NULL, &thread_monitor);
	timer_settime (thread_monitor, 0, &value, NULL);
}

/*********************************************************************
*	Function: StopTimer
*
*	Purpose:  This function simply stops the timer
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   Cited to newbie14 at www.daniweb.com
*
********************************************************************/
void StopTimer(void)
{
	struct itimerspec value;

	value.it_value.tv_sec = 0;
	value.it_value.tv_nsec = 0;

	value.it_interval.tv_sec = 0;
	value.it_interval.tv_nsec = 0;

	timer_settime (thread_monitor, 0, &value, NULL);
}

/*********************************************************************
*	Function: ThreadCheck
*
*	Purpose:  This function will check each critical thread to ensure 
*				that is has been activated the minimum amount 
*				of times. If it hasn't, a system critical error has been 
*				encountered, and it will go into emergency shutdown
*
*	Inputs:    
*			1) int sig: this is the signal that was triggered
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void ThreadCheck(int sig)
{
	if (PT_calculate_hit <= 4 && system_up)
	{
		fputs("ThreadCheck: Calculation Thread critical error... Haulting system\n", file_descriptor);
		PushMessage("ThreadCheck: Calculation Thread critical error... Haulting system\n");
		FCS_data.command = critical;
	}

	if (PT_send_hit <= 4 && system_up)
	{
		fputs("ThreadCheck: Send Thread critical error. Haulting system\n", file_descriptor);
		SendUART("ThreadCheck: Calculation Thread critical error... Haulting system\n");
		FCS_data.command = critical;
	}

	if (PT_pilot_hit <= 4 && system_up)
	{
		fputs("ThreadCheck: Pilot Thread critical error. Haulting system\n", file_descriptor);
		SendUART("ThreadCheck: Pilot Thread critical error... Haulting system\n");
		FCS_data.command = critical;
	}

	if (PT_sensor_hit <= 4 && system_up)
	{
		fputs("ThreadCheck: Sensor Thread critical error. Haulting system\n", file_descriptor);
		SendUART("ThreadCheck: Sensor Thread critical error... Haulting system\n");
		FCS_data.command = critical;
	}
    
    /*if (PT_test_hit <= 4 && system_up)
    {
        fputs("ThreadCheck: Test Thread critical error. Haulting system\n", file_descriptor);
		SendUART("ThreadCheck: Test Thread critical error... Haulting system\n");
		FCS_data.command = critical;
    }*/
		
	PT_calculate_hit 	= 0;
	PT_send_hit 		= 0;
	PT_pilot_hit 		= 0;
	PT_sensor_hit 		= 0;
    PT_test_hit         = 0;
}

/*********************************************************************
*	Function: UARTSetup()
*
*	Purpose:  This function simply sets up and initializes the UART
*				port of the PI Zero
*
*	Inputs:      N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void UARTSetup()
{
	uart_connection = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY);
	
	if (uart_connection == -1)
	{
		fputs("ERROR - UART setup failed... Entering CRITICAL\n", file_descriptor);
		printf("ERROR - UART setup failed... Entering CRITICAL\n");
		FCS_data.command = critical;
	}

	else
	{
		fputs("SUCCES - UART setup complete...\n", file_descriptor);
		printf("SUCCESS UART setup complete...\n");
	}
	
	struct termios options;
	
	tcgetattr(uart_connection, &options);
	options.c_cflag = B115200| CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(uart_connection, TCIFLUSH);
	tcsetattr(uart_connection, TCSANOW, &options);
}

/*********************************************************************
*	Function: SendUART
*
*	Purpose:  This function will send data out through UART
*
*	Inputs:      
*			1) char * data_str: this is the string of data that is to be
*				sent out through the UART
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void SendUART(char * data_str)
{
	if (uart_connection != - 1)
	{
		printf("Sending: %s", data_str);

		int size = write(uart_connection, data_str, strlen(data_str));
		
		if (size < 0)
		{
			fputs("ERROR - UART TX did not send correctly... Initializing CRITICAL\n", file_descriptor);
			printf("ERROR - UART TX did not send correctly... Initializing CRITICAL\n");
			FCS_data.command = critical;
		}
	}

	else
	{
		fputs("ERROR - Filestream has failed... Initializing CRITICAL\n", file_descriptor);
		printf("ERROR - Filestream has failed... Initializing CRITICAL\n");
		FCS_data.command = critical;
	}
}

/*********************************************************************
*	Function: ThreadSetup()
*
*	Purpose:  This function simply sets up the system threads and check
*				for success
*
*	Inputs:      N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void ThreadSetup()
{
	int thread_error = 0;

	//This is the thread will blocking wait for data to be sent from the user terminal
	thread_error = pthread_create(&send_message, NULL, &PT_SendMessage, NULL); 
	
	if (thread_error != 0)
	{
		fputs("ERROR - Send Thread not created... Initiating CRITICAL\n", file_descriptor);
		SendUART("ERROR - Send Thread not created... Initiating CRITICAL\n");
		FCS_data.command = critical;
	}

	//Give time for send thread to initialize
	delay(5);

	//This is the thread that will constantly take the received data, smooth it,
		//then store it in the global array sensor_data[]
	thread_error = pthread_create(&calculate_data, NULL, &PT_CalculateDistance, NULL);

	if (thread_error != 0)
	{
		fputs("ERROR - Calculate Thread not created... Initiating CRITICAL\n", file_descriptor);
		PushMessage("ERROR - Calculate Thread not created... Initiating CRITICAL\n");
		FCS_data.command = critical;
	}
	
	//This is the thread will blocking wait for data to be sent from the user terminal
	thread_error = pthread_create(&receive_data, NULL, &PT_ReceiveData, NULL); 
	
	if (thread_error != 0)
	{
		fputs("ERROR - Receive Thread not created... Initiating CRITICAL\n", file_descriptor);
		PushMessage("ERROR - Receive Thread not created... Initiating CRITICAL\n");
		FCS_data.command = critical;
	}

	//This thread will pilot the flight controller
	thread_error = pthread_create(&pilot, NULL, &PT_Pilot, NULL); 
	
	if (thread_error != 0)
	{
		fputs("ERROR - Pilot Thread not created... Initiating CRITICAL\n", file_descriptor);
		PushMessage("ERROR - Pilot Thread not created... Initiating CRITICAL\n");
		FCS_data.command = critical;
	}

	//This thread sends sensor information to the terminal
	thread_error = pthread_create(&send_sensor, NULL, &PT_SensorData, NULL); 
	
	if (thread_error != 0)
	{
		fputs("ERROR - Sensor Thread not created... Initiating CRITICAL\n", file_descriptor);
		PushMessage("ERROR - Sensor Thread not created... Initiating CRITICAL\n");
		FCS_data.command = critical;
	}
    
	//This thread sends sensor information to the terminal
	thread_error = pthread_create(&test, NULL, &PT_TestThread, NULL); 
	
	if (thread_error != 0)
	{
		fputs("ERROR - Test Thread not created... Initiating CRITICAL\n", file_descriptor);
		PushMessage("ERROR - Test Thread not created... Initiating CRITICAL\n");
		FCS_data.command = critical;
	}
}

/*********************************************************************
*	Function: PushMessage
*
*	Purpose:  This function will add a new node to the linked list
*
*	Inputs:      
*					1) char * message: this is the message that will be added to 
*						the FCS message queue
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void PushMessage(char * message)
{
	//If list is empty
	if (m_head == NULL)
	{	
		m_head = (struct FCS_message *)malloc(sizeof(struct FCS_message));
		m_head->next = NULL;
		
		m_head->message = (char *)malloc(strlen(message) + 1);
		strcpy(m_head->message, message);
		m_tail = m_head;
	}
	
	else
	{
		m_tail->next = (struct FCS_message *)malloc(sizeof(struct FCS_message));
		m_tail = m_tail->next;
		m_tail->next = NULL;
		m_tail->message = (char *)malloc(strlen(message) + 1);
		strcpy(m_tail->message, message);
	}	
	
	nodes_in_list++;
}

/*********************************************************************
*	Function: PopMessage
*
*	Purpose:  This function will remove the oldest node in the list
*
*	Inputs:      
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void PopMessage()
{
	struct FCS_message * temp = NULL;

	if (m_head != NULL)
	{
		if (m_head == m_tail)
			m_tail = m_head->next;
	
		temp = m_head;
		m_head = m_head->next;
	
		free(temp->message);
		free(temp);
		nodes_in_list--;
	}
}

/*******************************************************************
*	Function: FileSetup
*
*	Purpose: This funuction sets up a data log file based on 
*			the curren time
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void FileSetup()
{
	time (&raw_time);
	time_info = localtime(&raw_time);

	char * file_name = (char * )malloc(strlen("/home/pi/Documents/Projects/FCS/Data Logs/") + strlen(asctime(time_info)) + strlen("txt"));
	strcpy(file_name, "/home/pi/Documents/Projects/FCS/Data Logs/");
	strcat(file_name, asctime(time_info));
	file_name[strlen(file_name) - 1] = '.';
	strcat(file_name, "txt");

	file_descriptor = fopen(file_name, "w+");

	if (file_descriptor == 0)
	{
		PushMessage("ERROR - Data log could not be opened... Entering CRITICAL\n");
		FCS_data.command = critical;
	}	

	else
	{	
		fputs("SUCCESS - Data log opened...\n", file_descriptor);
		PushMessage("SUCCESS - Data log opened...\n");
		delay(5);
	}
}

/*******************************************************************
*	Function: PWMSetup
*
*	Purpose: This function sets up the pins for PWM output
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void PWMSetup()
{
	fputs("SYSTEM: Initializing PWM...\n", file_descriptor);
	PushMessage("SYSTEM: Initializing PWM...\n");

	//Channel 5 [ARM]
	system("gpio mode 21 out");
	system("gpio write 21 0");

	//Channel 1 [YAW]
	system("gpio mode 25 out");
	system("gpio write 25 0");

	//Channel 2 [PITCH]
	system("gpio mode 27 out");
	system("gpio write 27 0");

	//Channel 3 [THROTTLE]
	system("gpio mode 22 out");
	system("gpio write 22 0");

	//Channel 4 [ROLL]
	system("gpio mode 6 out");
	system("gpio write 6 0");

	//Channel 5 [ARM]
	softPwmCreate(arm, 0, 200);
	softPwmWrite(arm, 10);	
	delay(100);

	//Channel 1 [YAW]
	softPwmCreate(yaw, 0, 200);
	softPwmWrite(yaw, 15);	
	delay(100);

	//Channel 2 [PITCH]
	softPwmCreate(pitch, 0, 200);
	softPwmWrite(pitch, 15);	
	delay(100);

	//Channel 3 [THROTTLE]
	softPwmCreate(throttle, 0, 200);
	softPwmWrite(throttle, 10);	
	delay(100);

	//Channel 4 [ROLL]
	softPwmCreate(roll, 0, 200);
	softPwmWrite(roll, 15);	
	delay(100);
}

/*******************************************************************
*	Function: Arm
*
*	Purpose: This function arm the flight controller
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void Arm()
{   
    if (!critical_set)
    {
        fputs("SYSTEM: Arming flight controller...\n", file_descriptor);
        PushMessage("SYSTEM: Arming flight controller..\n");

        softPwmWrite(arm, 15);	
        softPwmWrite(yaw, 15);
        softPwmWrite(pitch, 15);
        softPwmWrite(throttle, 10);
        softPwmWrite(roll, 15);
        
        armed = 1;
        disarmed = 0;
    }
}

/*******************************************************************
*	Function: Disarm
*
*	Purpose: This function disarms the flight controller
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void Disarm()
{
	fputs("SYSTEM: Disarming flight controller...\n", file_descriptor);
	PushMessage("SYSTEM: Disarming flight controller...\n");

	softPwmWrite(arm, 10);	
	softPwmWrite(yaw, 15);
	softPwmWrite(pitch, 15);
	softPwmWrite(throttle, 10);
	softPwmWrite(roll, 15);	

	armed 		= 0;
	disarmed 	= 1;
}

/*******************************************************************
*	Function: SetPWM
*
*	Purpose: This function will set a channels PWM
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void SetPWM(int pin, int duty_cycle)
{
	softPwmWrite(pin, duty_cycle);
}

/*******************************************************************
*	Function: CommandHandler
*
*	Purpose: This function will handle incoming messaged from the
*		ground control user terminal
*
*	Inputs:	 	N/A
*
*	Outputs: 	N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*******************************************************************/
void CommandHandler(char * command)
{
	char * token 		= NULL;
	char * ptr			= NULL;
	int conversion_1 	= 0;
	int conversion_2	= 0;
	int count 			= 0;

	token = strtok(command, " ");

	printf("Command received: %s\n", command);

	//If Arm command
	if (strcmp(token, "Arm") == 0)
	{
		fputs("SYSTEM: Arm command accepted...\n", file_descriptor);
		PushMessage("SYSTEM: Arm command accepted...\n");
		Arm();
	}

	//If Disarm command
	else if (strcmp(token, "Disarm") == 0)
	{
		fputs("SYSTEM: Disarm command accepted...\n", file_descriptor);
		PushMessage("SYSTEM: Disarm command accepted...\n");
		Disarm();
	}

	//If SetPWM command
	else if (strcmp(token, "SetPWM") == 0)
	{
		token = strtok(NULL, " ");

		//If there is another parameter
		if (token != NULL)
		{
			conversion_1 = atoi(token);
			printf("Conversion 1: %d\n", conversion_1);

			//If conversion is equal to a valid pin
			if (conversion_1 == 21  || conversion_1 == 22)
			{
				token = strtok(NULL, " ");

				//If second parameter exists
				if (token != NULL)
				{
					conversion_2 = atoi(token);
					printf("Conversion 2: %d\n", conversion_2);

					//If the value is between 0 and 9
					if (conversion_2 > 0 && conversion_2 < 10)
					{
						fputs("SYSTEM: SetPWM command accepted...\n", file_descriptor);
						PushMessage("SetPWM: SetPWM command accepted...\n");
						SetPWM(conversion_1, (conversion_2 + 10));
					}

					//Not a valid value
					else
					{
						fputs("SYSTEM: SetPWM command rejected... Value is not within range [0:9]\n", file_descriptor);		
						PushMessage("SYSTEM: SetPWM command rejected... Value is not within range [0:9]\n");
					}
				}
								
				//Else missing PWM value
				else
				{
					fputs("SYSTEM: SetPWM command rejected... Missing PWM value\n", file_descriptor);
					PushMessage("SYSTEM: SetPWM command rejected... Missing PWM value\n");
				}
			}
			
			//Not a valid pin
			else
			{
				fputs("SYSTEM: SetPWM command rejected... Not a valid pin\n", file_descriptor);
				PushMessage("SYSTEM: SetPWM command rejected... Not a valid pin\n");
			}
		}
		
		//Else if data is received from the FCS
		else if (strcmp (token, "DATA") == 0)
		{
			if (!in_test)
			{
				fputs("SYSTEM: Test initiated...\n", file_descriptor);
				PushMessage("SYSTEM: Test initiated...\n");
			}
			
			//Let the system know this is a test
			in_test = 1;
		
			for (count = 0; count < 7; count++)
				FCS_data.prioritiy[count] = atoi(strtok(NULL, " "));
	
		}
		
		//Else if the test has been stopped
		else if (strcmp(token, "END_TEST") == 0)
		{
			
			fputs("SYSTEM: Test haulted...\n", file_descriptor);
			PushMessage("SYSTEM: Test haulted...\n");
			
			in_test = 0;
		}
		
		//This will initiate the takeoff sequence 
		else if (strcmp(token, "TAKEOFF" ) == 0 )
			FCS_data.command = takeoff;
		
		//This will initiate the land sequence
		else if (strcmp(token, "LAND") == 0)
			FCS_data.command = land;
		
		//This will initiate the e_land sequence
		else if (strcmp(token, "E_LAND") == 0)
			FCS_data.command = e_land;
		
		//Else missing pin and value parameter
		else
		{
			fputs("SYSTEM: SetPWM command missing pin number...\n", file_descriptor);
			PushMessage("SYSTEM: SetPWM command missing pin number..\n");
		}
	}
}

/*******************************************************************
*	Function: SensorDataString
*
*	Purpose: This function will compile a list of all the sensors
*		data and then send it over
*
*	Inputs:
*			1): char * sensor_string: This is the sensor
*			string values
*
*	Outputs: 	
*			1) char * sensor_string: This is the sensor
*			string values
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
char * SensorDataString(char * sensor_string)
{
	char buffer[4];
	memset(buffer, 0, sizeof(buffer));
	
	int count = 0;
				
	//Set initializer
	sensor_string[0] = '*';
	sensor_string[1] = ';';

	for (count = 0; count < 7; count++)
	{
		sprintf(buffer, "%d", (int)sensor_data[count]);
		strcat(sensor_string, buffer);
		strcat(sensor_string, ";");
	}
		
	strcat(sensor_string, "\n");

	return sensor_string;
}

/*******************************************************************
*	Function: FindDirection
*
*	Purpose: This function will find the path of least resistance
*           based on the passed in axis/ It will check in the order
*           of front -> right -> left -> back -> up -> down
*
*	Inputs:     
*               1) int axis: This is the axis in reference (x, y, z, +/-)
*
*	Outputs:    
*               1) int direction: This is the axis to take that has
*               the lease resistance for the referenced axis
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*       0 axis = Y+, 1 axis = X+, 2 axis = X-, 3 axis = Y-, 
*       4 axis = Z+, 5 axis = Z-
*
*******************************************************************/
int FindDirection(int axis)
{
    int direction   = 0;
    int value       = 6; //Highest priority, find your way down to the lowest priority
    
    //If not Y+, find highest Y+ value
    if (axis != 0 && value > 2)
    {
        value = FCS_data.priority[front_l];
        
        if (value > FCS_Data.priority[front_r])
            value = FCS_Data.priority[front_r];
        
        direction = 0;
    }
    
    //If not X+, check right priority
    if (axis != 1 && value > 2)
    {
        if (value > FCS_data.priority[right])
        {
            value = FCS_data.priority[right];
            direction = 1;
        }
    }
    
    //If not X-, check left priority
    if (axis != 2 && value > 2)
    {
        if (value > FCS_data.priority[left])
        {
            value = FCS_data.priority[left];
            direction = 2;
        }
    }
    
    //If not rear sensor, check rear priority
    if (axis != 3 && value > 2)
    {
        if (value > FCS_data.priority[rear])
        {
            value = FCS_data.priority[rear];
            direction = 3;
        }
    }
    
    //If we did not find a direction on the X/Y axis that is clear...
    if (value > 2)
    {
        //If not Z+ axis, check positive sensor
        if (axis != 4)
        {
            if (value > FCS_data.priority[posi])
            {
                value = FCS_data.priority[posi];
                direction = 4;
            }
        }
        
        //If not Z-, check negative sensor
        if (axis != 5)
        {
            if (value > FCS_data.priority[nega])
            {
                value = FCS_data.priority[nega];
                direction = 5;
            }
        }
    }
    
    return direction;
}

/*******************************************************************
*	Function: MotorControl
*
*	Purpose: This function will be in charge of the actual PWMing 
*           to the flight controller based on the input direction
*
*	Inputs:     
*               1) int direction: This is used to determine which way  
*               to stear the FCS
*
*	Outputs:    N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		
*       0 axis = Y+, 1 axis = X+, 2 axis = X-, 3 axis = Y-, 
*       4 axis = Z+, 5 axis = Z-
*
*******************************************************************/
void MotorControl(int direction)
{
    switch (direction)
    {
        //Forward
        case 0:
            PushMessage("*Ahead");
        break;
        
        //Right
        case 1:
            PushMessage("*Starboard");
        break;
        
        //Left
        case 2:
            PushMessage("*Port");
        break;
        
        //Backwards
        case 3:
            PushMessage("*Astern");
        break;
        
        //Up
        case 4:
            PushMessage("*Positive");
        break;
        
        //Down
        case 5:
            PushMessage("*Negative");
        break;
        
        default:
            fputs("SYSTEM: Invalid flight direction... Entering CRITICAL\n");
            PushMessage("SYSTEM: Invalid flight direction... Entering CRITICAL\n");
            FCS_data.command = critical;
        break;
    }
}

/*******************************************************************
*	Function: EXE_StandBy
*
*	Purpose: This function will direct the FCS based on the inputs
*           and keep the FCS in STandBy Mode
*
*	Inputs:     N/A
*
*	Outputs:    N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void EXE_StandBy()
{    
    if (!disarmed)
    {
        Disarm();
        fputs("SYSTEM: Executing StandBy...\n", file_descriptor);
        PushMessage("SYSTEM: Executing StandBy...\n");
    }
    
    PushMessage("*StandBy");
}

/*******************************************************************
*	Function: EXE_TakeOff
*
*	Purpose: This function guide the quadcopter based on inputs from 
*           the takeoff state machines
*
*	Inputs:     N/A
*
*	Outputs:    N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void EXE_TakeOff()
{
    if (!armed && FCS_data.positive)
    {
        Arm();
        fputs("SYSTEM: Executing TakeOff...\n", file_descriptor);
        PushMessage("SYSTEM: Executing TakeOff...\n");
    }
    
    PushMessage("*Ascending"); 
    
}

/*******************************************************************
*	Function: EXE_Maneuver
*
*	Purpose: This will commpute the evasive manuvers the FCS needs
*           to make to evade while hovering
*
*	Inputs:     N/A
*
*	Outputs:    N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*******************************************************************/
void EXE_Maneuver()
{
    int X_priority      = 0;
    int X_champion      = 0;
    int X_Pdirection    = 0;
    int X_Ndirection    = 0;
    
    int Y_priority      = 0;
    int Y_champion      = 0;
    int Y_Pdirection    = 0;
    int Y_Ndirection    = 0;
    int Y_holder        = 0;
    
    int Z_priority      = 0;
    int Z_champion      = 0;
    int Z_Pdirection    = 0;
    int Z_Ndirection    = 0;
    
    ///////////////////////////////////////////////////////
    //This stage of EXE_Maneuver will find the best direction 
    //to fly for each command issued
    
    //If command to go left
    if (FCS_data.port)
        X_Pdirection = FindDirection(1);
    
    //If command to go right
    if (FCS_data.starboard)
        X_Ndirection = FindDirection(2);
    
    //If command to go forward
    if (FCS_data.ahead)
        Y_Ndirection = FindDirection(3);
    
    //If command to go backwards
    if (FCS_data.astern)
        Y_Pdirection= FindDirection(0);
    
    //If command to go up
    if (FCS_data.positive)
        Z_Ndirection = FindDirection(5);
    
    //If command to go down
    if (FCS_data.negative)
        Z_Pdirection = FindDirection(4);
    
    ///////////////////////////////////////////////////////
    //This stage of the function will calculate whether 
    //the positive of negative of each axis is higer
    
    //If command to go Left or right, pick the highest
    if (FCS_data.port || FCS_data)
    {
        if (FCS_data.priority[right] >= FCS_data.priority[left])
        {
            X_champion = 0;
            X_priority = FCS_data.priority[right];
        }
        
        else
        {
            X_champion = 1;
            X_priority = FCS_data.priority[left];
        }
    }
    
    //If a command to go forward or reverse, pick the highest
    if (FCS_data.astern || FCS_data.ahead)
    {
        if (FCS_data.priority[front_l] >= FCS_data.priority[front_r])
            Y_holder = FCS_data.priority[front_l];
        
        else
            Y_holder = FCS_data.priority[front_r];
        
        //If front priority is higher then rear priority, front wins
        if (holder >= FCS_data.priority[rear])
        {
            Y_champion = 0;
            Y_priority = holder;
        }
        
        else
        {
            Y_champion = 1;
            Y_priority = FCS_data.priority[rear];
        }
    }
    
    //If a command to go up or down
    if (FCS_data.positive || FCS_data.negative)
    {
        if (FCS_data.priority[posi] >= FCS_data.priority[nega])
        {
            Z_champion = 0;
            Z_priority = FCS_data.priority[posi];
        }
        
        else 
        {
            Z_champion = 1;
            Z_priority = FCS_data.priority[nega];
        }
    }
    
    ///////////////////////////////////////////////////////
    //The final stage determines which of the axis has 
    //priority and then takes action based on its direction
    
    //If the X axis has the largest priority
    if (X_priority >= Y_priority && X_priority >= Z_priority)
    {
        //This is used to determine if + or - 
        if (!X_champion)
            MotorControl(X_Pdirection);
        
        else
            MotorControl(X_Ndirection);
    }
    
    //If the Y axis has the largest priority
    else if ( Y_priority >= X_priority && Y_priority >= Z_priority)
    {
        //This is to determine if + or -
        if (!Y_champion)
            MotorControl(Y_Pdirection);
        
        else
            MotorControl(Y_Ndirection);
    }
    
    //Else Z axis has highest priority
    else
    {
        //This is tp determine if + or -
        if (!Z_champion)
            MotorControl(Z_Pdirection);
        
        else
            MotorControl(Z_Ndirection);
    }
}


/*******************************************************************
*	Function: EXE_Land
*
*	Purpose: This will commpute the landing procedure and supply the
*           the command to the flight controller
*
*	Inputs:     N/A
*
*	Outputs:    N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		
*		priority[0] = front left sensor
*		priority[1] = left sensor
*		priority[2] = rear sensor
*		priority[3] = positive sensor (+z axis)
*		priority[4] = front right sensor
*		priortiy[5] = right sensor
*		priority[6] = negative sensor (-z axis)
*
*******************************************************************/
void EXE_Land()
{
    PushMessage("*Descending");
}

/*******************************************************************
*	Function: EXE_ELand
*
*	Purpose: This will commpute the emergency landing procedure and
*           supply the command to the flight controller
*
*	Inputs:     N/A
*
*	Outputs:    N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void EXE_ELand()
{
    PushMessage("*E_Descend");
}

/*******************************************************************
*	Function: EXE_Critical
*
*	Purpose: This will disarm the FCS and stay in critical
*
*	Inputs:     N/A
*
*	Outputs:    N/A
*
*	Variables: 	N/A
*
*	Bugs:		N/A
*
*	Notes: 		N/A
*
*******************************************************************/
void EXE_Critical()
{
    if (!disarmed)
    {
        PushMessage("SYSTEM: System disarmed... Continuing CRITICAL\n");
        fputs("SYSTEM: System disarmed... Continuing CRITICAL\n", file_descriptor);
    }
    
    Disarm();
    critical_set = 1;
    PushMessage("*CRITICAL");
}

/*********************************************************************
*	Function: PT_CalculateDistance
*
*	Purpose:  This thread will calculate and smooth data coming
*			in from the sensors, then storing it in 
*			sensor_data[]
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void * PT_CalculateDistance(void * arg)
{
	PT_calculate_up = 1;
	int count = 0;
	int nasty = 0;
	float holder;
	float data[7][10] = {{0.0}};
	
	fputs("SYSTEM: Calculate Thread created...\n", file_descriptor);
	PushMessage("SYSTEM: Calculate Thread created...\n");

	while(system_up && PT_calculate_up)
	{
		DataLoading();

		for (count = 0; count < 7; count++)
		{
			for (nasty = 9; nasty > 0; nasty--)
				data[count][nasty] = data[count][nasty - 1];

			data[count][0] = SPI_buffer[count] * 0.019608;
			data[count][0] = data[count][0] / 0.00957;

			sensor_data[count] = ((data[count][0] * 25) + (data[count][1] * 20) + (data[count][2] * 15) 
				+ (data[count][3] * 10) + (data[count][4] * 5) + (data[count][5] * 5) 
				+ (data[count][6] * 5) + (data[count][7] * 5) + (data[count][8] * 5) 
				+ (data[count][9] * 5)) / 100;			
		}

		PriorityComputation();

		PT_calculate_hit++;
	}

	fputs("SYSTEM: Calculate thread exiting...\n", file_descriptor);
	PushMessage("SYSTEM: Calculate thread exiting...\n");
	PT_calculate_up = 0;
	pthread_exit(NULL);
}

/*********************************************************************
*	Function: PT_RecieveData
*
*	Purpose:   This thread will block on the RX UART line and wait for 
*				a message from the XBEE module
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void * PT_ReceiveData(void * arg)
{
	PT_receive_up = 1;
	
	int read_status = 0;
	int reading = 1;
	char buffer[256];
	char command[256];
	memset(buffer, 0, sizeof(buffer));
	
	fputs("SYSTEM: Receive Thread created...\n", file_descriptor);
	PushMessage("SYSTEM: Receive Thread created...\n");
	
	while (system_up && PT_receive_up)
	{
		reading = 1;

		while (reading)
		{
			read_status = read(uart_connection, buffer, sizeof(buffer));
			

			if (read_status < 0)
			{
				fputs("ERROR - Failure to read off of serial port... Initializing CRITICAL\n", file_descriptor);
				PushMessage("ERROR - Failure to read off of serial port... Initliazing CRITICAL\n");
				FCS_data.command = critical;
			}
	
			else
			{

				if (buffer[strlen(buffer) - 1] == '*')
					reading = 0;

				strcat(command, buffer);
				memset(buffer, 0, sizeof(buffer));
			}
		}

		command[strlen(command) - 1] = '\0';
		printf("Command: %s\n", command);
		
		CommandHandler(command);
		memset(command, '\0', sizeof(command));
	}
	
	fputs("SYSTEM: Receive thread exiting...\n", file_descriptor);
	PushMessage("SYSTEM: Receive thread exiting...\n");
	PT_receive_up = 0;
	pthread_exit(NULL);	
}

/*********************************************************************
*	Function: PT_SendMessage()
*
*	Purpose:  This thread will send the mesages queued up to the user terminal
*
*	Inputs:    N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void * PT_SendMessage(void * arg)
{
	PT_send_up = 1;

	fputs("SYSTEM: Send Thread created...\n", file_descriptor);
	PushMessage("SYSTEM: Send Thread created...\n");

	while (PT_send_up)
	{
		if (nodes_in_list)
		{
			SendUART(m_head->message);
			PopMessage();
			delay(20);
		}

		PT_send_hit++;
	}

	fputs("SYSTEM: Send thread exiting...\n", file_descriptor);
	SendUART("SYSTEM: Send thread exiting...\n");
	pthread_exit(NULL);
}

/*********************************************************************
*	Function: PT_Pilot
*
*	Purpose:  This thread is in charge of using the issued commands
*				and priorities to decide where to fly the quadcopter
*
*	Inputs:      N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void * PT_Pilot(void * arg)
{
	PT_pilot_up = 1;

	fputs("SYSTEM: Pilot Thread created...\n", file_descriptor);
	PushMessage("SYSTEM: Pilot Thread created...\n");

	while (PT_pilot_up && system_up)
	{
        switch (FCS_data.command)
        {
            case standby:
                EXE_StandBy();
            break;
            
            case takeoff: 
                EXE_TakeOff();
            break;
            
            case hover:
                EXE_Maneuver();
            break;
            
            case flight:
            break;
            
            case land:
                EXE_Land();
            break;
            
            case e_land:
                EXE_ELand();
            break;
            
            case critical:
            
                while(1)
                    EXE_Critical();
                
            break;
            
            default:
                fputs("SYSTEM: Invalid flight state... Initializing CRITICAL\n", file_descriptor);
                PushMessage("SYSTEM: Invalid flight state... Initializing CRITICAL\n");
                
                while(1)
                    EXE_Critical();
                
            break;
        }	
		PT_pilot_hit++;
	}

	fputs("SYSTEM: Pilot thread exiting...\n", file_descriptor);
	PushMessage("SYSTEM: Pilot thread exiting...\n");
	PT_pilot_up = 0;
	pthread_exit(NULL);
}

/*********************************************************************
*	Function: PT_SensorData
*
*	Purpose:  This thread is sends the sensor information to the 
*		ground terminal
*
*	Inputs:      N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void * PT_SensorData(void * arg)
{
	PT_sensor_up = 1;
	char sensor_string[50];

	fputs("SYSTEM: Sensor Thread created...\n", file_descriptor);
	PushMessage("SYSTEM: Sensor Thread created...\n");

	while (PT_sensor_up && system_up)
	{
		memset(sensor_string, 0, sizeof(sensor_string));

		//Send the data values from the sensors
		//PushMessage(SensorDataString(sensor_string));	
		delay(100);		

		PT_sensor_hit++;
	}

	fputs("SYSTEM: Sensor thread exiting...\n", file_descriptor);
	PushMessage("SYSTEM: Sensor thread exiting...\n");
	PT_sensor_up = 0;
	pthread_exit(NULL);
}

/*********************************************************************
*	Function: PT_TestThread
*
*	Purpose:  This thread is used for software simulation and testing
*
*	Inputs:      N/A
*
*	Outputs:   N/A
*
*	Variables: N/A
*
*	Bugs:	   N/A
*
*	Notes:	   N/A
*
********************************************************************/
void * PT_TestThread(void * arg)
{
    PT_test_up 1;
	
	char buffer[256] = {'\0'};
    
    while (PT_test_up && system_up)
    {		
    }
}